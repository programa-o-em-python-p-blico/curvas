from PyQt5.QtWidgets import QMessageBox
from PyQt5 import QtGui


class MyMessageBox(QMessageBox):
    def __init__(self, _windowTitle, _messageText, _self):
        super(MyMessageBox, self).__init__(_self)
    
        self.setWindowTitle(_windowTitle)
        self.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
        self.setText(_messageText)
        self.setIcon(self.Warning)
        self.setStandardButtons(self.Ok)
        