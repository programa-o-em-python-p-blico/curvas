# Curvas

Programa com interface gráfica para a realização de aproximação de curvas matemáticas. Dois métodos são utilizados: O Método dos Mínimos Quadrados (MMQ) e o Polinômio Interpolador de Newton.

#

>![](arquivos/interface1.PNG) 

>![](arquivos/interface2.PNG)

>![](arquivos/interface3.PNG)

>![](arquivos/interface4.PNG)
