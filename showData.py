from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, QComboBox, QPushButton, QRadioButton, QVBoxLayout, QFormLayout, QGroupBox, QScrollArea
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QDoubleValidator
from myMessgeBox_Warning import MyMessageBox
import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt
import numpy as np
from libMatrix import *


class MyGraphic(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        self.title = 'Configuração gráfico'
        left = 650
        top = 250 
        width = 200
        height = 100
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.grid = QGridLayout()

        self.title_label = QLabel('Título: ')

        self.title_lineEdit = QLineEdit()
        self.title_lineEdit.setFixedSize(120, 20)

        self.legendx_label = QLabel('Legenda de x: ')

        self.legendx_lineEdit = QLineEdit()
        self.legendx_lineEdit.setFixedSize(120, 20)

        self.legendy_label = QLabel('Legenda de y: ')

        self.legendy_lineEdit = QLineEdit()
        self.legendy_lineEdit.setFixedSize(120, 20)

        self.cores = ['Azul', 'Branco', 'Verde', 'Vermelho', 'Amarelo', 'Laranja', 'Marrom', 'Preto']
        self.colors = ['blue', 'white', 'green', 'red', 'yellow', 'orange', 'brown', 'black']

        self.colorCurve_label = QLabel('Cor da curva: ')

        self.colorCurve_comboBox = QComboBox()
        self.colorCurve_comboBox.addItems(self.cores)

        self.colorPoints_label = QLabel('Cor dos pontos: ')

        self.colorPoints_comboBox = QComboBox()
        self.colorPoints_comboBox.addItems(self.cores)

        self.interval_label = QLabel('Intervalo: ')
        self.inf_lineEdit = QLineEdit()
        self.inf_lineEdit.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.inf_lineEdit.setFixedSize(50, 20)

        self.label = QLabel(' : ')
        self.label.setFixedSize(6, 20)

        self.sup_lineEdit = QLineEdit()
        self.sup_lineEdit.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.sup_lineEdit.setFixedSize(50, 20)

        self.showPoints_radioButton = QRadioButton('Não mostrar os pontos')
        self.showPoints_radioButton.toggled.connect(self.disablePoints)
        self.showPoints = True

        self.makeGraphic_button = QPushButton('Gerar gráfico')
        self.makeGraphic_button.setFixedSize(90, 30)
        self.makeGraphic_button.clicked.connect(self.generateGraphic)

        self.grid.addWidget(self.title_label, 0, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.title_lineEdit, 0, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.legendx_label, 1, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendx_lineEdit, 1, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.legendy_label, 2, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendy_lineEdit, 2, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.interval_label, 3, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.inf_lineEdit, 3, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.label, 3, 2, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.sup_lineEdit, 3, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.colorCurve_label, 4, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.colorCurve_comboBox, 4, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.colorPoints_label, 5, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.colorPoints_comboBox, 5, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.showPoints_radioButton, 6, 1, 1, 4, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.makeGraphic_button, 7, 1, 1, 3, QtCore.Qt.AlignCenter)

        self.setLayout(self.grid)

        self.show()

    def disablePoints(self, _isChecked):
        if _isChecked:
            self.colorPoints_comboBox.setEnabled(False)
            self.colorPoints_label.setEnabled(False)
        else:
            self.colorPoints_comboBox.setEnabled(True)
            self.colorPoints_label.setEnabled(True)
        
        self.showPoints = not _isChecked

    def generateGraphic(self):
        for i, cor in enumerate(self.cores):
            if self.colorCurve_comboBox.currentText() == cor:
                self.color_grafico = self.colors[i]
            if self.colorPoints_comboBox.currentText() == cor:
                self.color_pontos = self.colors[i]

        graphicOptions = {
            'graphicTitle': self.title_lineEdit.text(),
            'legendx': self.legendx_lineEdit.text(),
            'legendy': self.legendy_lineEdit.text(),
            'inf': self.inf_lineEdit.text().replace(',', '.'),
            'sup': self.sup_lineEdit.text().replace(',', '.'),
            'colorGraph': self.color_grafico,
            'colorPoints': self.color_pontos
        }

        if self.inf_lineEdit.text() != '' and self.sup_lineEdit.text() != '':
            if float(self.inf_lineEdit.text()) < float(self.sup_lineEdit.text()):
                self.hide()
                if self.myWidget.title == 'Curva - Interpolação por Newton':
                    self.plotGraphicNewton(graphicOptions)
                else:
                    self.plotGraphicMMQ(graphicOptions)
            else:
                # Message Box
                message = MyMessageBox('Atenção', 'O segundo valor do intervalo deve\n ser maior que o primeiro.', self)
                message.show()
                return 0
        else:
            self.hide()
            if self.myWidget.title == 'Curva - Interpolação por Newton':
                self.plotGraphicNewton(graphicOptions)
            else:
                self.plotGraphicMMQ(graphicOptions)

    def plotGraphicNewton(self, op):
        if  op['inf'] == '':
            op['inf'] = min(self.myWidget.xlist)
        else:
            op['inf'] = float(op['inf'])
        if  op['sup'] == '':
            op['sup'] = max(self.myWidget.xlist)
        else:
            op['sup'] = float(op['sup'])

        x1 = np.arange(op['inf'], op['sup'] + 0.01, 0.01)
        x2 = self.myWidget.xlist[:]
        y2 = self.myWidget.ylist[:]
        cont = 0
        for i in range(0, len(x2)):
            if x2[cont] > op['sup'] or x2[cont] < op['inf']:
                del x2[cont]
                del y2[cont]
            else:
                cont += 1

        sols = list()
        ###########
        for array in self.myWidget.seq:
            print(len(array))
        ###########
        for valor in x1:
            produto = 1
            sol = 0
            for i, item in enumerate(self.myWidget.seq):
                sol += item[0] * produto
                produto *= (valor - self.myWidget.xlist[i])
                if i == 0:
                    sol = item[0]
            sols.append(sol)
        
        try:
            self.myPlot.close()
        except:
            pass
        
        self.myPlot = plt
        self.myPlot.style.use(self.myWindow.tema)
        self.myPlot.plot(x1, sols, color=op['colorGraph'])
        if self.showPoints:
            self.myPlot.scatter(x2, y2, color=op['colorPoints'])
        self.myPlot.title(op['graphicTitle'], color=self.myWindow.cortitulo, fontsize=self.myWindow.fontsizetitle)
        self.myPlot.xlabel(op['legendx'], color=self.myWindow.corx, fontsize=self.myWindow.fontsizex)
        self.myPlot.ylabel(op['legendy'], color=self.myWindow.cory, fontsize=self.myWindow.fontsizey)
        fig = self.myPlot.gcf()
        fig.canvas.set_window_title('Gráfico ' + self.myWidget.title.split()[4])
        self.myPlot.show()
        
    def plotGraphicMMQ(self, op):
        if  op['inf'] == '':
            op['inf'] = min(self.myWidget.xlist)
        else:
            op['inf'] = float(op['inf'])
        if  op['sup'] == '':
            op['sup'] = max(self.myWidget.xlist)
        else:
            op['sup'] = float(op['sup'])

        x1 = np.arange(op['inf'], op['sup'] + 0.01, 0.01)
        x2 = self.myWidget.xlist[:]
        y2 = self.myWidget.ylist[:]
        cont = 0
        for i in range(0, len(x2)):
            if x2[cont] > op['sup'] or x2[cont] < op['inf']:
                del x2[cont]
                del y2[cont]
            else:
                cont += 1
        sols = list()

        for valor in x1:
            resp = 0

            for i in range(0, self.myWidget.numCoefficients()):
                if 'exponenciação' in self.myWidget.terms[i]:
                    fi = fun1(valor, self.myWidget.terms[i][1])
                elif 'exponencial' in self.myWidget.terms[i]:
                    fi = fun2(valor)
                elif 'log_neperiano' in self.myWidget.terms[i]:
                    fi = fun3(valor)
                elif 'logaritmo' in self.myWidget.terms[i]:
                    fi = fun4(valor, self.myWidget.terms[i][1])
                resp += self.myWidget.sol[i] * fi
            sols.append(resp)
        
        try:
            self.myPlot.close()
        except:
            pass
        
        self.myPlot = plt
        self.myPlot.style.use(self.myWindow.tema)
        self.myPlot.plot(x1, sols, color=op['colorGraph'])
        if self.showPoints:
            self.myPlot.scatter(x2, y2, color=op['colorPoints'])
        self.myPlot.title(op['graphicTitle'], color=self.myWindow.cortitulo, fontsize=self.myWindow.fontsizetitle)
        self.myPlot.xlabel(op['legendx'], color=self.myWindow.corx, fontsize=self.myWindow.fontsizex)
        self.myPlot.ylabel(op['legendy'], color=self.myWindow.cory, fontsize=self.myWindow.fontsizey)
        fig = self.myPlot.gcf()
        fig.canvas.set_window_title('Gráfico ' + self.myWidget.title.split()[2])
        self.myPlot.show()

    def setMyWindow_and_MyWidget(self, _myWindow, _myWidget):
        self.myWindow = _myWindow
        self.myWidget = _myWidget


class ShowCoef(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Saída'
        left = 650 
        top = 250 
        width = 180
        height = 200
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setLayout(self.vbox)
        self.cont = 0

    def setData(self, _sol, _decimals):
        self.cont += 1
        if self.cont > 1:
            self.vbox.itemAt(0).widget().setParent(None)

        form = QFormLayout()

        GroupBox = QGroupBox('  Coeficientes  ')

        Scroll = QScrollArea()

        for i, coef in enumerate(_sol):
            label1 = QLabel(f'a{i} :  ')
            resultado = f'{_sol[i]:.{_decimals}f}'
            label2 = QLabel(resultado.replace('.', ','))
            form.addRow(label1, label2)
        GroupBox.setLayout(form)
        GroupBox.setMinimumWidth(140)
        if len(_sol) > 9:
            Scroll.setWidget(GroupBox)
            Scroll.setFixedSize(170, 180)
            self.vbox.addWidget(Scroll)
        else:
            self.vbox.addWidget(GroupBox)
