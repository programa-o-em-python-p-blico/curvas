def validador_float(currentLineEdit):
    if currentLineEdit.text() != '':
        if len(currentLineEdit.text()) > 1:
            if ',' in currentLineEdit.text()[:-1] or '.' in currentLineEdit.text()[:-1]:
                if not currentLineEdit.text()[-1] in '0123456789':
                    currentLineEdit.setText(currentLineEdit.text()[:-1])
            else:
                if not currentLineEdit.text()[-1] in ',.0123456789':
                    currentLineEdit.setText(currentLineEdit.text()[:-1])
        else:
            if not currentLineEdit.text()[-1] in '-0123456789':
                currentLineEdit.setText(currentLineEdit.text()[:-1])