from PyQt5.QtWidgets import QMainWindow, QAction, QMessageBox
from PyQt5 import QtGui
from myWidget import MyWidget
from myStackedWidget import MyStackedWidget
from configurationAndSuport import *
import json


class MyWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.mainTitle = 'Curvas'
        self.icon = 'arquivos/icone.ico'
        self.left = 500
        self.top = 100
        self.width = 480
        self.height = 480

        self.setWindowTitle(self.mainTitle)
        self.setWindowIcon(QtGui.QIcon(self.icon))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setAutoFillBackground(True)

        # Open general configuration
        try:
            archieve_json = open('configuration.json', 'r')
            self.configuration = json.load(archieve_json)
        except:
            self.configuration = {
                'sistema': {
                    'decimais': 3,
                    'tema': 'Fusion'
                },
                'grafico': {
                    'tema': 'seaborn-whitegrid',
                    'cortitulo': 'black',
                    'corx': 'black',
                    'cory': 'blaxk',
                    'fontsizetitulo': 14,
                    'fontsizex': 12,
                    'fontsizey': 12
                }
                    }

        # Sistem configuration 
        self.decimal = self.configuration['sistema']['decimais']
        self.systemTema = self.configuration['sistema']['tema']

        # Graphic configuration
        self.tema = self.configuration['grafico']['tema']
        self.cortitulo = self.configuration['grafico']['cortitulo']
        self.corx = self.configuration['grafico']['corx']
        self.cory = self.configuration['grafico']['cory']
        self.fontsizetitle = self.configuration['grafico']['fontsizetitulo']
        self.fontsizex = self.configuration['grafico']['fontsizex']
        self.fontsizey = self.configuration['grafico']['fontsizey']

        # Menu Bar
        menuBar = self.menuBar()

            # Menu configuração
        confMenu = menuBar.addMenu('Configurações')

        decimal = QAction(QtGui.QIcon('arquivos/decimal.png'), 'Casas decimais', self)
        decimal.setShortcut('Ctrl+D')
        decimal.triggered.connect(self.decimalPlaces)
        confMenu.addAction(decimal)

        graphic = QAction(QtGui.QIcon('arquivos/grafico3.ico'), 'Gráfico', self)
        graphic.setShortcut('Ctrl+G')
        graphic.triggered.connect(self.graphicConf)
        confMenu.addAction(graphic)

        theme = QAction(QtGui.QIcon('arquivos/theme.ico'), 'Tema', self)
        theme.setShortcut('Ctrl+T')
        theme.triggered.connect(self.themeConf)
        confMenu.addAction(theme)

            # Menu ajuda
        helpMenu = menuBar.addMenu('Ajuda')

        suport = QAction(QtGui.QIcon('arquivos/suporte.png'), 'Suporte', self)
        suport.setShortcut('Ctrl+H')
        suport.triggered.connect(self.suporte)
        helpMenu.addAction(suport)

        self.myStackedWidget = MyStackedWidget()
        self.setCentralWidget(self.myStackedWidget)

        # Send the instance
        self.myStackedWidget.set_myWindow(self)

    def decimalPlaces(self):
        self.dialogDecimal = Decimals(self)
        self.dialogDecimal.show()

    def applyDecimal(self):
        if self.dialogDecimal.uma.isChecked():
            self.decimal = 1
        elif self.dialogDecimal.duas.isChecked():
            self.decimal = 2
        elif self.dialogDecimal.tres.isChecked():
            self.decimal = 3
        elif self.dialogDecimal.quatro.isChecked():
            self.decimal = 4
        elif self.dialogDecimal.cinco.isChecked():
            self.decimal = 5
        else:
            self.decimal = 3
        self.configuration['sistema']['decimais'] = self.decimal

        self.updateConfigurations()

        self.dialogDecimal.close()

    def graphicConf(self):
        self.graphicConfiguration = GraphicConf(self)
        self.graphicConfiguration.show()

    def applyGraphicConf(self):
        self.tema = self.graphicConfiguration.themes_box.currentText()
        self.cortitulo = self.graphicConfiguration.colors[self.graphicConfiguration.title1_box.currentIndex()]
        self.corx = self.graphicConfiguration.colors[self.graphicConfiguration.x1_box.currentIndex()]
        self.cory = self.graphicConfiguration.colors[self.graphicConfiguration.y1_box.currentIndex()]
        self.fontsizetitle = int(self.graphicConfiguration.title2_box.currentText())
        self.fontsizex = int(self.graphicConfiguration.x2_box.currentText())
        self.fontsizey = int(self.graphicConfiguration.y2_box.currentText())
        
        self.configuration['grafico']['tema'] = self.tema
        self.configuration['grafico']['cortitulo'] = self.cortitulo
        self.configuration['grafico']['corx'] = self.corx
        self.configuration['grafico']['cory'] = self.cory
        self.configuration['grafico']['fontsizetitulo'] = self.fontsizetitle
        self.configuration['grafico']['fontsizex'] = self.fontsizex
        self.configuration['grafico']['fontsizey'] = self.fontsizey

        self.updateConfigurations()

        self.graphicConfiguration.close()
    
    def themeConf(self):
        self.theme = Theme(self)
        self.theme.show()
    
    def applyThemeConf(self):
        if self.theme.windows.isChecked():
            self.systemTema = 'Windows'
        elif self.theme.windowsvista.isChecked():
            self.systemTema = 'windowsvista'
        else:
            self.systemTema = 'Fusion'
        
        self.configuration['sistema']['tema'] = self.systemTema

        self.updateConfigurations()

        self.theme.close()

        message = QMessageBox.information(self, 'Informativo ',
                                        'O tema só será aplicado após reiniciar o programa.')

    def suporte(self):
        self.suport = Suport(self)
        self.suport.show()
    
    def closeEvent(self, event):
        event.ignore()
        questionClose = QMessageBox(self)
        questionClose.setIcon(QMessageBox.Question)
        questionClose.setWindowTitle('Finalização')
        questionClose.setText('Deseja realmente fechar a aplicação?')
        questionClose.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        buttonSim = questionClose.button(QMessageBox.Yes)
        buttonSim.setText('Sim')
        buttonNao = questionClose.button(QMessageBox.No)
        buttonNao.setText('Não')
        answer = questionClose.exec()
  
        if answer == QMessageBox.Yes:
            exit(0)
    
    def updateConfigurations(self):
        configuration = json.dumps(self.configuration, indent=4, sort_keys=False)
        archieve_json = open('configuration.json', 'w')
        archieve_json.write(configuration)
        archieve_json.close()
