from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QPushButton, QLabel,
                             QGroupBox, QLineEdit, QGridLayout)
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPixmap, QDoubleValidator
from myMessgeBox_Warning import MyMessageBox
from insertData import InsertPoints, DefineEquation
from showData import MyGraphic, ShowCoef
from libMatrix import *


class MMQ(QWidget):
    def __init__(self):
        super(MMQ, self).__init__()
        self.title = 'Curva - MMQ'

        self.vbox = QVBoxLayout()
        self.setLayout(self.vbox)

        self.goBack_button = QPushButton()
        self.goBack_button.setIcon(QtGui.QIcon('arquivos/voltar3.ico'))
        self.goBack_button.setIconSize(QtCore.QSize(90, 40))
        self.goBack_button.setMaximumSize(30, 30)
        self.vbox.addWidget(self.goBack_button)

        self.methodName_label = QLabel('Método dos mínimos quadrados')
        self.methodName_label.setFont(QtGui.QFont('Corbel Light', 13))
        self.methodName_label.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop)
        self.methodName_label.setMaximumHeight(80)
        self.vbox.addWidget(self.methodName_label)

        self.dialogMmq()
        self.solutionMmq()

        self.fi_list = None
        self.constant_list = None
        self.xlist = None
        self.ylist = None
        self.terms = None
        self.sol = None

    def dialogMmq(self):
        self.grid1 = QGridLayout()

        self.grid2 = QGridLayout()

        self.vbox2 = QVBoxLayout()

        self.inputData_groupBox = QGroupBox(' Entrada de dados ')

        self.equationFormat_label = QLabel('Formato da equação:   ')

        self.image_label = QLabel()
        pixmap = QPixmap('arquivos/formatoT.png')
        self.image_label.setPixmap(pixmap)

        self.n_label = QLabel('n:  ')
        self.n_label.setFont(QtGui.QFont('Cambria Math', 11))
        self.n_label.setStyleSheet('color:green')

        self.n_lineEdit = QLineEdit()
        self.n_lineEdit.setFixedSize(60, 23)
        self.n_lineEdit.setFocus()
        self.n_lineEdit.textChanged.connect(self.validatorInt)

        self.defineEquation_button = QPushButton('Definir equação')
        self.defineEquation_button.setFixedSize(120, 25)
        self.defineEquation_button.clicked.connect(self.defineEquation)

        self.grid1.addWidget(self.equationFormat_label, 0, 0)
        self.grid1.addWidget(self.image_label, 0, 1, QtCore.Qt.AlignLeft)
        self.grid1.setAlignment(QtCore.Qt.AlignLeft)

        self.numPoints_label = QLabel('Número de pontos:  ')

        self.numPoints_lineEdit = QLineEdit()
        self.numPoints_lineEdit.setMinimumSize(20, 25)
        self.numPoints_lineEdit.setFocus()
        self.numPoints_lineEdit.setFixedSize(60, 23)
        self.numPoints_lineEdit.textChanged.connect(self.validatorInt)

        self.aux1_label = QLabel()
        self.aux1_label.setMinimumSize(80, 25)

        self.inputPoints_button = QPushButton('Inserir pontos')
        self.inputPoints_button.setMinimumSize(120, 25)
        self.inputPoints_button.clicked.connect(self.insertPoints)

        self.grid2.addWidget(self.n_label, 0, 0, 1, 1, QtCore.Qt.AlignRight)
        self.grid2.addWidget(self.n_lineEdit, 0, 1, QtCore.Qt.AlignLeft)
        self.grid2.addWidget(self.defineEquation_button, 0, 2, QtCore.Qt.AlignRight)

        self.grid2.addWidget(self.numPoints_label, 1, 0, QtCore.Qt.AlignRight)
        self.grid2.addWidget(self.numPoints_lineEdit, 1, 1,  QtCore.Qt.AlignLeft)
        self.grid2.addWidget(self.inputPoints_button, 1, 2, QtCore.Qt.AlignRight)

        self.vbox2.addItem(self.grid1)
        self.vbox2.addItem(self.grid2)
        self.inputData_groupBox.setLayout(self.vbox2)
        
        self.vbox.addWidget(self.inputData_groupBox)
    
    def solutionMmq(self):
        grid1 = QGridLayout()

        grid2 = QGridLayout()
        grid2.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()

        self.group = QGroupBox(' Solução numérica ')
        self.group.setMaximumHeight(120)

        self.showCoef_button = QPushButton('Exibr coeficientes')
        self.showCoef_button.setFixedSize(120, 25)
        self.showCoef_button.clicked.connect(self.exibirCoef)

        self.f_label = QLabel('        f ( ')
        self.f_label.setMaximumWidth(40)

        self.valorDeX_lineEdit = QLineEdit()
        self.valorDeX_lineEdit.setFixedSize(50, 20)
        self.valorDeX_lineEdit.textChanged.connect(self.showSolution)

        self.equal_label = QLabel('  )  =   ')

        self.solution_label = QLabel()
        self.solution_label.setFixedSize(120, 20)
        self.solution_label.setStyleSheet('background-color:rgb(255, 255, 255)')
        self.solution_label.setAlignment(QtCore.Qt.AlignCenter)

        label4 = QLabel()
        label4.setFixedSize(80, 100)

        self.remake_button = QPushButton('Refazer')
        self.remake_button.setFixedSize(120, 30)
        self.remake_button.clicked.connect(self.refazer)

        self.graphic_button = QPushButton('Gerar gráfico')
        self.graphic_button.setFixedSize(150, 30)
        self.graphic_button.clicked.connect(self.plotar_grafico)

        grid1.addWidget(self.showCoef_button, 0, 5)
        grid1.addWidget(self.f_label, 2, 0)
        grid1.addWidget(self.valorDeX_lineEdit, 2, 1)
        grid1.addWidget(self.equal_label, 2, 2)
        grid1.addWidget(self.solution_label, 2, 3)
        grid1.addWidget(label4, 2, 4)
        grid2.addWidget(self.graphic_button, 0, 0)
        grid2.addWidget(self.remake_button, 0, 1)

        vbox.addItem(grid1)
        self.group.setLayout(vbox)

        self.vbox.addWidget(self.group)
        self.vbox.addLayout(grid2)
        self.valorDeX_lineEdit.setFocus()

    def numCoefficients(self):
        return int(self.n_lineEdit.text())
    
    def numPoints(self):
        return int(self.numPoints_lineEdit.text())

    def defineEquation(self):
        if self.n_lineEdit.text() != '':
            define_equation = DefineEquation(self.numCoefficients(), self.myWindow.decimal, self)
            define_equation.setCurrentWidget(self)
            define_equation.show()
        else:
            # Message Box
            message = MyMessageBox('Atenção', 'Por favor, informe a quantidade de coeficientes.', self)
            message.show()

            self.n_lineEdit.setFocus()
            return 0
    
    def setEquation(self, _fi_list, _constant_list):
        self.fi_list = _fi_list
        self.constant_list = _constant_list

    def insertPoints(self):
        if self.numPoints_lineEdit.text() != '':
            if self.numPoints() < int(self.n_lineEdit.text()):
                # Message Box
                message = MyMessageBox('Antenção', 'O número de pontos deve ser maior ou igual\nao número de coeficientes a resolver.', self)
                message.show()

                self.numPoints_lineEdit.clear()
                self.numPoints_lineEdit.setFocus()
                return 0
            insert_points = InsertPoints(self.numPoints(), self.myWindow.decimal, self)
            insert_points.setCurrentWidget(self)
            insert_points.show()
        else:
            if self.n_lineEdit.text() == '':
                # Maessage Box
                message = MyMessageBox('Antenção', 'Por favor, primeiro defina a equação.', self)
                message.show()

                self.n_lineEdit.setFocus()
                return 0
            else:
                # Message Box
                message = MyMessageBox('Antenção', 'Po favor informe o número de pontos.', self)
                message.show()

                self.numPoints_lineEdit.setFocus()
                return 0
    
    def setPoints(self, _xlist, _ylist):
        self.xlist = _xlist
        self.ylist = _ylist

    def showSolution(self):
        if self.xlist != None and self.fi_list != None:
            termos = list()
            for i, eq in enumerate(self.fi_list):
                aux = list()

                if eq == 'x^α':
                    tipo = 'exponenciação'
                    exp = self.constant_list[i]
                    aux.append(tipo)
                    aux.append(exp)
                    termos.append(aux[:])
                elif eq == 'e^x':
                    tipo = 'exponencial'
                    termos.append(tipo)
                elif eq == 'ln(x)':
                    tipo = 'log_neperiano'
                    termos.append(tipo)
                elif eq == 'logα(x)':
                    tipo = 'logaritmo'
                    base = self.constant_list[i]
                    aux.append(tipo)
                    aux.append(base)
                    termos.append(aux[:])

            matrix = makeMatrixMMQ(self.xlist, self.numCoefficients(), termos)
            vector = makeVectorMMQ(self.xlist, self.ylist, self.numCoefficients(), termos)
            triangularization(matrix, vector)

            sol = retrosubstitution(matrix, vector)
            self.terms = termos[:]
            self.sol = sol[:]

            if self.valorDeX_lineEdit.text() != '' and self.valorDeX_lineEdit.text() != '-':
    
                instância = float((self.valorDeX_lineEdit.text()).replace(',', '.'))
                resp = 0
        
                for i in range(0, self.numCoefficients()):
                    if 'exponenciação' in termos[i]:
                        fi = fun1(instância, termos[i][1])
                    elif 'exponencial' in termos[i]:
                        fi = fun2(instância)
                    elif 'log_neperiano' in termos[i]:
                        fi = fun3(instância)
                    elif 'logaritmo' in termos[i]:
                        fi = fun4(instância, termos[i][1])
                    resp += self.sol[i] * fi
        
                resp = f'{resp:.{self.myWindow.decimal}f}'
                self.solution_label.setText(resp.replace('.', ','))

        else:
            if self.valorDeX_lineEdit.text() != '':
                # Message Box
                message = MyMessageBox('Atenção', 'Por favor, certifique-se de ter entrado\n com todos os dados necessários.', self)
                message.show()

                self.valorDeX_lineEdit.setText('')
                if self.n_lineEdit.text() == '':
                    self.n_lineEdit.setFocus()
                elif self.numPoints_lineEdit.text() == '':
                    self.numPoints_lineEdit.setFocus()
                else:
                    self.defineEquation_button.setFocus()

    def plotar_grafico(self):
        if self.xlist != None and self.fi_list != None:
            try:
                self.funcao()
                self.myGraphic = MyGraphic(self)
                self.myGraphic.setMyWindow_and_MyWidget(self.myWindow, self)
            except:
                # Message Box
                message = MyMessageBox('Antenção', 'Por favor, certifique-se de ter entrado\ncom todos os dados necessários.', self)
                message.show()

                if self.n_lineEdit.text() == '':
                    self.n_lineEdit.setFocus()
                elif self.numPoints_lineEdit.text() == '':
                    self.numPoints_lineEdit.setFocus()
                else:
                    self.graphic_button.setFocus()
                return 0
        else:
            # Message Box
            message = MyMessageBox('Antenção', 'Por favor, certifique-se de ter entrado\ncom todos os dados necessários.', self)
            message.show()
            self.valorDeX_lineEdit.setText('')
            if self.n_lineEdit.text() == '':
                self.n_lineEdit.setFocus()
            elif self.numPoints_lineEdit.text() == '':
                self.numPoints_lineEdit.setFocus()
            else:
                self.graphic_button.setFocus()
            return 0

    def funcao(self):
        self.showSolution()
        termos = list()

        for i, eq in enumerate(self.fi_list):
            aux = list()
            if eq == 'x^α':
                tipo = 'exponenciação'
                exp = self.constant_list[i]
                aux.append(tipo)
                aux.append(exp)
                termos.append(aux[:])
            elif eq == 'e^x':
                tipo = 'exponencial'
                termos.append(tipo)
            elif eq == 'ln(x)':
                tipo = 'log_neperiano'
                termos.append(tipo)
            elif eq == 'logα(x)':
                tipo = 'logaritmo'
                base = self.constant_list[i]
                aux.append(tipo)
                aux.append(base)
                termos.append(aux[:])

        matriz = makeMatrixMMQ(self.xlist, self.numCoefficients(), termos)
        vetor = makeVectorMMQ(self.xlist, self.ylist, self.numCoefficients(), termos)
        triangularization(matriz, vetor)
        self.sol = retrosubstitution(matriz, vetor)

    def refazer(self):
        self.fi_list = None
        self.constant_list = None
        self.xlist = None
        self.ylist = None
        self.terms = None
        self.sol = None
        self.n_lineEdit.clear()
        self.numPoints_lineEdit.clear()
        self.valorDeX_lineEdit.clear()
        self.solution_label.clear()
        self.n_lineEdit.setFocus()
        
    def exibirCoef(self):
        self.showSolution()
 
        try:         
            self.showCoef = ShowCoef(self)
            self.showCoef.setData(self.sol, self.myWindow.decimal)
            self.showCoef.show()

            self.valorDeX_lineEdit.setFocus()
        except:
            # Message Box
            message = MyMessageBox('Atenção', 'Por favor, certifique-se de ter entrado\ncom todos os dados necessários.', self)
            message.show()

            if self.n_lineEdit.text() == '':
                self.n_lineEdit.setFocus()
            elif self.numPoints_lineEdit.text() == '':
                self.numPoints_lineEdit.setFocus()
            else:
                self.defineEquation_button.setFocus()
            return 0

    def validatorInt(self):
        self.currentLineEdit = self.sender()

        if self.currentLineEdit.text() != '':
            if len(self.currentLineEdit.text()) > 1 and len(self.currentLineEdit.text()) < 4:
                if not self.currentLineEdit.text()[-1] in '0123456789':
                    self.currentLineEdit.setText(self.currentLineEdit.text()[:-1])
            else:
                if not self.currentLineEdit.text()[-1] in '123456789':
                    self.currentLineEdit.setText(self.currentLineEdit.text()[:-1])
                else:
                    if len(self.currentLineEdit.text()) > 3:
                        self.currentLineEdit.setText(self.currentLineEdit.text()[:-1])
    
    def validatorDouble(self):
        currentLineEdit = self.sender()
        currentLineEdit.setValidator(QDoubleValidator(-9999999, 9999999, self.myWindow.decimal))
    
    def setMyWindow(self, _myWindow):
        self.myWindow = _myWindow
        self.valorDeX_lineEdit.setValidator(QDoubleValidator(-9999999, 9999999, self.myWindow.decimal))

    
