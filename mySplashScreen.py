from PyQt5.QtWidgets import QSplashScreen, QLabel, QProgressBar
from PyQt5.QtGui import QPixmap
from PyQt5 import QtCore, QtGui
import pygame
import time


class MySplash(QSplashScreen):
    def __init__(self, _app):
        super().__init__()
        self.app = _app
        self.setPixmap(QPixmap('arquivos/Splash2.jpg'))

        barra = QLabel(self)
        barra.setGeometry(500, 10, 200, 2)
        barra.setStyleSheet('background-color:lightblue')

        icone = QPixmap('arquivos/logo8.png')
        icon_label = QLabel(self)
        icon_label.setStyleSheet('background-color:white')
        icon_label.setPixmap(icone)
        icon_label.setGeometry(0, 20, 370, 111)
        icon_label.setAlignment(QtCore.Qt.AlignCenter)

        version_label = QLabel(self)
        version_label.setText('Versão 1.1, Maio de 2020')
        version_label.setGeometry(15, 450, 150, 20)

        author_label = QLabel(self)
        author_label.setText('R. Vieira')
        author_label.setFont(QtGui.QFont('Bookman Old Style', 10))
        author_label.setGeometry(600, 450, 100, 20)
       
        DEFAULT_STYLE = """
        QProgressBar{
            border: 0px solid grey;
            border-radius: 0px;
            text-align: center
        }

        QProgressBar::chunk {
            background-color: lightblue;
            width: 0.5px;
            margin: 0px;
        }
        """

        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(0, 430, 692, 5)
        self.progressBar.setTextVisible(False)
        self.progressBar.setStyleSheet(DEFAULT_STYLE)
        
        self.mysong = pygame
        self.mysong.init()
        self.mysong.mixer.music.load('arquivos/initialization.mp3')
        self.mysong.mixer.music.set_volume(0.5)
            
    def execute(self):
        self.show()
        for i in range(0, 100):
            self.progressBar.setValue(i)
            t = time.time()
            while time.time() < t + 0.03:
                self.app.processEvents()
        self.mysong.mixer.music.play()
    
    def mousePressEvent(self, event):
        event.ignore()