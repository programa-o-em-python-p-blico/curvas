from PyQt5.QtWidgets import QApplication
import sys
from myWindow import MyWindow
from mySplashScreen import MySplash


if __name__ == '__main__':
    App = QApplication([])
    mySplash = MySplash(App)
    mySplash.execute()
    myWindow = MyWindow()
    mySplash.finish(myWindow)
    App.setStyle(myWindow.systemTema)
    myWindow.show()
    sys.exit(App.exec_())
