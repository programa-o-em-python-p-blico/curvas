from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QLabel, QGroupBox, QLineEdit, QGridLayout, QMessageBox
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QDoubleValidator, QIntValidator
from insertData import InsertPoints
from myMessgeBox_Warning import MyMessageBox
from showData import MyGraphic
import time


class Newton(QWidget):
    def __init__(self):
        super(Newton, self).__init__()
        self.title = 'Curva - Interpolação por Newton'

        self.vbox = QVBoxLayout()
        self.setLayout(self.vbox)
        
        self.goBack_button = QPushButton()
        self.goBack_button.setIcon(QtGui.QIcon('arquivos/voltar3.ico'))
        self.goBack_button.setIconSize(QtCore.QSize(90, 40))
        self.goBack_button.setMaximumSize(30, 30)
        self.vbox.addWidget(self.goBack_button)

        self.methodName_label = QLabel('Interpoção numérica pelo método de Newton')
        self.methodName_label.setFont(QtGui.QFont('Corbel Light', 13))
        self.methodName_label.setAlignment(QtCore.Qt.AlignCenter)
        self.methodName_label.setMaximumHeight(80)

        self.vbox.addWidget(self.methodName_label)

        self.dialogNewton()
        self.solutionNewton()

        self.xlist = None
        self.ylist = None
        self.seq = None

    def dialogNewton(self):
        self.hbox = QHBoxLayout()

        self.inputData_groupBox = QGroupBox(' Entrada de dados ')
        self.inputData_groupBox.setMaximumHeight(120)

        self.numPontos_label = QLabel('Número de pontos:')
        self.numPontos_label.setMinimumSize(100, 25)

        self.numPontos_lineEdit = QLineEdit()
        self.numPontos_lineEdit.textChanged.connect(self.validatorInt)
        self.numPontos_lineEdit.setFixedSize(60, 23)

        self.aux1_label = QLabel()
        self.aux1_label.setMinimumSize(80, 25)

        self.inputPoints_button = QPushButton('Inserir pontos')
        self.inputPoints_button.setMinimumSize(120, 25)
        self.inputPoints_button.clicked.connect(self.insertPoints)

        self.hbox.addWidget(self.numPontos_label)
        self.hbox.addWidget(self.numPontos_lineEdit)
        self.hbox.addWidget(self.aux1_label)
        self.hbox.addWidget(self.inputPoints_button)
        self.hbox.setAlignment(QtCore.Qt.AlignCenter)
        self.inputData_groupBox.setLayout(self.hbox)

        self.vbox.addWidget(self.inputData_groupBox)
    
    def solutionNewton(self):
        self.grid1 = QGridLayout()

        self.grid2 = QGridLayout()
        self.grid2.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()

        self.group = QGroupBox(' Solução numérica ')
        self.group.setMaximumHeight(120)

        self.f_label = QLabel('        f (')
        self.f_label.setMaximumWidth(40)

        self.valorDeX_lineEdit = QLineEdit()
        self.valorDeX_lineEdit.setFixedSize(50, 20)
        self.valorDeX_lineEdit.textChanged.connect(self.showSolution)

        self.equal_label = QLabel('  )  =')

        self.solution_label = QLabel()
        self.solution_label.setFixedSize(120, 20)
        self.solution_label.setStyleSheet('background-color:rgb(255, 255, 255)')
        self.solution_label.setAlignment(QtCore.Qt.AlignCenter)

        label4 = QLabel()
        label4.setFixedSize(200, 100)

        self.remake_button = QPushButton('Refazer')
        self.remake_button.setFixedSize(120, 30)
        self.remake_button.clicked.connect(self.refazer)

        self.graphic_button = QPushButton('Gerar gráfico')
        self.graphic_button.setFixedSize(150, 30)
        self.graphic_button.clicked.connect(self.plotar_grafico)

        self.grid1.addWidget(self.f_label, 0, 0)
        self.grid1.addWidget(self.valorDeX_lineEdit, 0, 1)
        self.grid1.addWidget(self.equal_label, 0, 2)
        self.grid1.addWidget(self.solution_label, 0, 3, QtCore.Qt.AlignLeft)
        self.grid1.addWidget(label4, 0, 4)
        self.grid2.addWidget(self.graphic_button, 0, 0)
        self.grid2.addWidget(self.remake_button, 0, 1)

        vbox.addItem(self.grid1)
        self.group.setLayout(vbox)

        self.vbox.addWidget(self.group)
        self.vbox.addLayout(self.grid2)
        self.valorDeX_lineEdit.setFocus()

    def numPoints(self):
        return int(self.numPontos_lineEdit.text())

    def insertPoints(self):
        if self.numPontos_lineEdit.text() != '':
            if self.numPontos_lineEdit.text() == '1':
                # Message Box
                message = MyMessageBox('Atenção', 'É necessário entrar com mais de um ponto.', self)
                message.show()
                self.numPontos_lineEdit.clear()
                self.numPontos_lineEdit.setFocus()
                return 0
            insert_points = InsertPoints(self.numPoints(), self.myWindow.decimal, self)
            insert_points.setCurrentWidget(self)
            insert_points.show()
        else:
            # Message Box
            message = MyMessageBox('Atenção', 'Por favor, informe o número de pontos.', self)
            message.show()
      
            self.numPontos_lineEdit.setFocus()
            return 0
    
    def setPoints(self, _xlist, _ylist):
        self.xlist = _xlist
        self.ylist = _ylist

    def showSolution(self):
        if self.valorDeX_lineEdit.text() != '' and self.valorDeX_lineEdit.text() != '-':
            if self.xlist != None:
                try:
                    aux1 = len(self.ylist) - 1
                    aux2 = 1
                    aux = list()
                    seq = list()
                    seq.append(self.ylist[:])

                    for c in range(0, len(self.ylist) - 1):
                        for i in range(0, aux1):
                            derivada = (seq[c][i + 1] - seq[c][i]) / (self.xlist[i + aux2] - self.xlist[i])
                            aux.append(derivada)
                        seq.append(aux[:])
                        aux.clear()
                        aux1 -= 1
                        aux2 += 1
               
                    instância = float((self.valorDeX_lineEdit.text()).replace(',', '.'))
                    produto = 1
                    sol = 0

                    for i, item in enumerate(seq):
                        sol += item[0] * produto
                        produto *= (instância - self.xlist[i])
                        if i == 0:
                            sol = item[0]
                    sol = f'{sol:.{self.myWindow.decimal}f}'
                    self.solution_label.setText(str(sol.replace('.', ',')))
                except:
                    # Message Box
                    message = MyMessageBox('Atenção', 'Por favor, entre com valores numéricos válidos.', self)
                    message.show()

                    self.valorDeX_lineEdit.clear()
                    self.valorDeX_lineEdit.setFocus()
            else:
                # Message Box
                message = MyMessageBox('Atenção', 'Por favor, primeiro\ninsira os pontos.', self)
                message.show()

                self.valorDeX_lineEdit.clear()
                self.solution_label.clear()
                if self.numPontos_lineEdit.text() == '':
                    self.numPontos_lineEdit.setFocus()
                else:
                    self.inputPoints_button.setFocus()

    def plotar_grafico(self):
        if self.numPontos_lineEdit.text() != '':
            try:
                self.funcao()
                self.myGraphic = MyGraphic(self)
                self.myGraphic.setMyWindow_and_MyWidget(self.myWindow, self)
            except:
                # Message Box
                message = MyMessageBox('Atenção', 'Por favor, certifique-se de ter entrado\ncom todos os dados necessários.', self)
                message.show()
                
                self.inputPoints_button.setFocus()
                return 0
        else:
            # Message Box
            message = MyMessageBox('Atenção', 'Por favor, primeiro\n insira os pontos.', self)
            message.show()
            self.numPontos_lineEdit.setFocus()
            return 0

    def funcao(self):
        aux1 = len(self.ylist) - 1
        aux2 = 1
        aux = list()
        seq = list()
        seq.append(self.ylist[:])

        for c in range(0, len(self.ylist) - 1):
            for i in range(0, aux1):
                derivada = (seq[c][i + 1] - seq[c][i]) / (self.xlist[i + aux2] - self.xlist[i])
                aux.append(derivada)
            seq.append(aux[:])
            aux.clear()
            aux1 -= 1
            aux2 += 1
        self.seq = seq[:]

    def refazer(self):
        self.numPontos_lineEdit.clear()
        self.valorDeX_lineEdit.clear()
        self.solution_label.clear()
        self.numPontos_lineEdit.setFocus()
        self.xlist = None
        self.ylist = None
        self.seq = None

    def validatorInt(self):
        if self.numPontos_lineEdit.text() != '':
            if len(self.numPontos_lineEdit.text()) > 1 and len(self.numPontos_lineEdit.text()) < 4:
                if not self.numPontos_lineEdit.text()[-1] in '0123456789':
                    self.numPontos_lineEdit.setText(self.numPontos_lineEdit.text()[:-1])
            else:
                if not self.numPontos_lineEdit.text()[-1] in '123456789':
                    self.numPontos_lineEdit.setText(self.numPontos_lineEdit.text()[:-1])
                else:
                    if len(self.numPontos_lineEdit.text()) > 3:
                        self.numPontos_lineEdit.setText(self.numPontos_lineEdit.text()[:-1])

    def setMyWindow(self, _myWindow):
        self.myWindow = _myWindow
        self.valorDeX_lineEdit.setValidator(QDoubleValidator(-9999999, 9999999, self.myWindow.decimal))
      
        