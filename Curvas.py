from PyQt5.QtWidgets import QApplication, QDialog, QVBoxLayout, QHBoxLayout, QPushButton, QWidget, QLabel, QMainWindow,\
                            QRadioButton, QProgressBar
from PyQt5.QtWidgets import QGridLayout, QGroupBox, QMenuBar, QLineEdit, QScrollArea, QFormLayout, QDialog, QMessageBox,\
                            QAction, QSplashScreen
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtGui import QPixmap, QIcon, QIntValidator, QDoubleValidator
from PyQt5 import QtGui
from PyQt5 import QtCore
import sys
from libMatrix import *
import time
import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt
import numpy as np
import pygame


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.MainTitle = 'Curvas'
        icon = 'arquivos/icone.ico'
        self.left = 500
        self.top = 100
        self.width = 480
        self.height = 480


        self.setWindowTitle(self.MainTitle)
        self.setWindowIcon(QtGui.QIcon(icon))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setAutoFillBackground(True)

        self.decimal = 3

        self.vetor1 = list() #auxílio para a criação dos gráficos
        self.vetor2 = list() #
        self.vetor3 = list() #
        self.vetor4 = list() #
        self.vetor5 = list() #
        self.vetor6 = list() #

        #self.pixmap = QPixmap('tema10.png')
        self.tema = 'seaborn-whitegrid'
        self.cortitulo = 'black'
        self.corx = 'black'
        self.cory = 'black'
        self.fontsizetitle = 12
        self.fontsizex = 12
        self.fontsizey = 12

        menuBar = self.menuBar()
        Confmenu = menuBar.addMenu('Configurações')
        decimal = QAction(QIcon('arquivos/decimal.png'), 'Casas decimais', self)
        decimal.setShortcut('Ctrl+D')
        decimal.triggered.connect(self.casas_decimais)
        Confmenu.addAction(decimal)
        Grafico = QAction(QIcon('arquivos/grafico3.ico'), 'Gráfico', self)
        Grafico.setShortcut('Ctrl+G')
        Grafico.triggered.connect(self.conf_grafico)
        Confmenu.addAction(Grafico)
        Ajudamenu = menuBar.addMenu('Ajuda')
        suporte = QAction(QIcon('arquivos/suporte.png'), 'Suporte', self)
        suporte.setShortcut('Ctrl+H')
        Ajudamenu.addAction(suporte)
        suporte.triggered.connect(self.suporte)

        self.show()

        a = self
        a.setCentralWidget(widget)
        

    def casas_decimais(self):
        decimal.show()

    def aplicar_decimal(self):
        if decimal.uma.isChecked():
            self.decimal = 1
        elif decimal.duas.isChecked():
            self.decimal = 2
        elif decimal.tres.isChecked():
            self.decimal = 3
        elif decimal.quatro.isChecked():
            self.decimal = 4
        elif decimal.cinco.isChecked():
            self.decimal = 5
        else:
            self.decimal = 3
        decimal.hide()

    def conf_grafico(self):
        conf_grafico.show()

    def aplicar_conf_grafico(self):
        self.tema = conf_grafico.boxTemas.currentText()
        self.cortitulo = conf_grafico.colors[conf_grafico.boxTitulo1.currentIndex()]
        self.corx = conf_grafico.colors[conf_grafico.boxx1.currentIndex()]
        self.cory = conf_grafico.colors[conf_grafico.boxy1.currentIndex()]
        self.fontsizetitle = int(conf_grafico.boxTitulo2.currentText())
        self.fontsizex = int(conf_grafico.boxx2.currentText())
        self.fontsizey = int(conf_grafico.boxy2.currentText())
        conf_grafico.hide()

    def suporte(self):
        suporte.show()


class Widget(QWidget):
    def __init__(self):
        super().__init__()
        self.textos_janela_principal()
        self.botões_janela_principal()
        #self.setFixedSize(480, 480)

    def textos_janela_principal(self):
        self.vbox = QVBoxLayout()
        text = QGridLayout()

        label1 = QLabel('Métodos numéricos:')
        label1.setFont(QtGui.QFont('Corbel Light', 16))
        label1.setMinimumSize(200, 70)
        label1.setAlignment(QtCore.Qt.AlignCenter)
        self.vbox.addWidget(label1)

        label2 = QLabel('Interpolação\n'
                        'por Newton ')
        label2.setFont(QtGui.QFont('Calibri Light', 11))
        label2.setStyleSheet('color:darkblue')
        label2.setMinimumSize(100, 60)

        label3 = QLabel()
        label3.setPixmap(QtGui.QPixmap('arquivos/arrow3.png'))
        label3.setMinimumSize(80, 50)
        label3.setAlignment(QtCore.Qt.AlignCenter)

        label4 = QLabel('Gera a curva que melhor se\n'
                        'aproxima dos pontos dados ')
        label4.setFont(QtGui.QFont('Calibri Light', 10))
        label4.setStyleSheet('color:black')
        label4.setMinimumSize(200, 60)

        label5 = QLabel('MMQ - Método\n'
                        'dos Mínimos\n'
                        'Quadrados')
        label5.setFont(QtGui.QFont('Calibri Light', 11))
        label5.setStyleSheet('color:darkblue')
        label5.setMinimumSize(100, 60)

        label6 = QLabel()
        label6.setPixmap(QtGui.QPixmap('arquivos/arrow3.png'))
        label6.setMinimumSize(80, 50)
        label6.setAlignment(QtCore.Qt.AlignCenter)

        label7 = QLabel('Gera os coeficientes da curva dada\n'
                        '(que descreve o fenômeno físico)\n'
                        'de forma que ela aproxime da melhor\n'
                        'forma possível os pontos dados')
        label7.setFont(QtGui.QFont('Calibri Light', 10))
        label7.setMinimumSize(200, 60)
        label7.setStyleSheet('color:black')

        text.addWidget(label2, 0, 0)
        text.addWidget(label3, 0, 1)
        text.addWidget(label4, 0, 2)
        text.addWidget(label5, 1, 0)
        text.addWidget(label6, 1, 1)
        text.addWidget(label7, 1, 2)

        label8 = QLabel('SELECIONE O PROCEDIMENTO DESEJADO')
        label8.setFont(QtGui.QFont('Eras Light ITC', 8))
        label8.setStyleSheet('color:black')
        label8.setMinimumSize(300, 40)
        label8.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignCenter)

        self.vbox.addItem(text)
        self.vbox.addWidget(label8)

        self.setLayout(self.vbox)

    def botões_janela_principal(self):
        button1 = QPushButton()
        button1.setText('Interpolação por Newton')
        button1.clicked.connect(self.open_janela_newton)
        button1.setFont(QtGui.QFont('Corbel Light', 18))
        button1.setMinimumSize(200, 70)

        button2 = QPushButton('MMQ')
        button2.clicked.connect(self.open_janela_MMQ)
        button2.setFont(QtGui.QFont('Corbel Light', 18))
        button2.setMinimumSize(200, 70)

        self.vbox.addWidget(button1)
        self.vbox.addWidget(button2)

    def open_janela_newton(self):
        self.n = Newton()
        window.setCentralWidget(self.n)
        window.setWindowTitle(newton.titleNewton)
        self.n.numPontos.setFocus()

    def open_janela_MMQ(self):
        self.m = MMQ()
        window.setCentralWidget(self.m)
        window.setWindowTitle(mmq.titleMMQ)
        self.m.n.setFocus()


class Newton(QWidget):
    def __init__(self):
        super().__init__()
        self.vbox = QVBoxLayout()
        self.setLayout(self.vbox)
        self.criar_janela_newton()
        self.botao_voltar()
        self.textos_newton()
        self.dialog_newton()
        self.solução_newton()

        self.setFixedSize(window.width, window.height)

    def criar_janela_newton(self):
        self.titleNewton = 'Curva - Interpolação por Newton'
        left = 150
        top = 150
        width = 470
        height = 460

        self.setGeometry(left, top, width, height)

    def botao_voltar(self):
        buttonVoltar = QPushButton()
        buttonVoltar.setIcon(QtGui.QIcon('arquivos/voltar3.ico'))
        buttonVoltar.setIconSize(QtCore.QSize(90, 40))
        buttonVoltar.setMaximumSize(30, 30)
        buttonVoltar.clicked.connect(self.voltar_menu)
        self.vbox.addWidget(buttonVoltar)

    def textos_newton(self):
        titulo = QLabel('Interpoção numérica pelo método de Newton')
        titulo.setFont(QtGui.QFont('Corbel Light', 13))
        titulo.setAlignment(QtCore.Qt.AlignCenter)
        titulo.setMaximumHeight(80)

        self.vbox.addWidget(titulo)

    def dialog_newton(self):
        hbox = QHBoxLayout()

        self.groupBox = QGroupBox(' Entrada de dados ')
        self.groupBox.setMaximumHeight(120)

        label = QLabel('Número de pontos:')
        label.setMinimumSize(100, 25)

        self.numPontos = QLineEdit()
        self.numPontos.textChanged.connect(self.validador_int_maior_que_um)
        self.numPontos.setFixedSize(60, 23)

        label2 = QLabel()
        label2.setMinimumSize(80, 25)

        inputPontos = QPushButton('Inserir pontos')
        inputPontos.setMinimumSize(120, 25)
        inputPontos.clicked.connect(self.inserir_pontos)

        hbox.addWidget(label)
        hbox.addWidget(self.numPontos)
        hbox.addWidget(label2)
        hbox.addWidget(inputPontos)
        hbox.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox.setLayout(hbox)

        self.vbox.addWidget(self.groupBox)

    def numero_pontos(self):
        return int(self.numPontos.text())

    def inserir_pontos(self):
        if self.numPontos.text() != '':
            inserir_pontos.dados(self.numero_pontos())
            inserir_pontos.show()
            self.valorDeX.clear()
            self.label3.clear()
            self.valorDeX.setFocus()
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, informe o número de pontos.')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            self.numPontos.setFocus()
            return 0

    def voltar_menu(self):
        w = Widget()
        window.setCentralWidget(w)
        window.setWindowTitle(window.MainTitle)

    def solução_newton(self):
        grid1 = QGridLayout()

        grid2 = QGridLayout()
        grid2.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()

        self.group = QGroupBox(' Solução numérica ')
        self.group.setMaximumHeight(120)

        label1 = QLabel('        f (')
        label1.setMaximumWidth(40)

        self.valorDeX = QLineEdit()
        self.valorDeX.setValidator(QDoubleValidator(-999999, 999999, 3))
        self.valorDeX.setMaximumSize(40, 20)
        self.valorDeX.textChanged.connect(self.mostrar_solucao)

        label2 = QLabel('  )  =')

        self.label3 = QLabel()
        self.label3.setFixedSize(120, 20)
        self.label3.setStyleSheet('background-color:rgb(255, 255, 255)')
        self.label3.setAlignment(QtCore.Qt.AlignCenter)

        label4 = QLabel()
        label4.setFixedSize(200, 100)

        buttonRefazer = QPushButton('Refazer')
        buttonRefazer.setFixedSize(120, 30)
        buttonRefazer.clicked.connect(self.refazer)

        buttonGrafico = QPushButton('Gerar gráfico')
        buttonGrafico.setFixedSize(150, 30)
        buttonGrafico.clicked.connect(self.plotar_grafico)

        grid1.addWidget(label1, 0, 0)
        grid1.addWidget(self.valorDeX, 0, 1)
        grid1.addWidget(label2, 0, 2)
        grid1.addWidget(self.label3, 0, 3, QtCore.Qt.AlignLeft)
        grid1.addWidget(label4, 0, 4)
        grid2.addWidget(buttonGrafico, 0, 0)
        grid2.addWidget(buttonRefazer, 0, 1)

        vbox.addItem(grid1)
        self.group.setLayout(vbox)

        self.vbox.addWidget(self.group)
        self.vbox.addLayout(grid2)
        self.valorDeX.setFocus()

    def mostrar_solucao(self):
        if self.valorDeX.text() != '' and self.valorDeX.text() != '-':
            if self.numPontos.text() != '':
                try:
                    self.validador_double()
                    self.pontosX = list()
                    self.pontosY = list()

                    for i in range(0, len(inserir_pontos.xlist)):
                        self.pontosX.append(float((inserir_pontos.xlist[i].text()).replace(',', '.')))
                        self.pontosY.append(float((inserir_pontos.ylist[i].text()).replace(',', '.')))
                    aux1 = len(self.pontosY) - 1
                    aux2 = 1
                    aux = list()
                    seq = list()
                    seq.append(self.pontosY[:])

                    for c in range(0, len(self.pontosY) - 1):
                        for i in range(0, aux1):
                            derivada = (seq[c][i + 1] - seq[c][i]) / (self.pontosX[i + aux2] - self.pontosX[i])
                            aux.append(derivada)
                        seq.append(aux[:])
                        aux.clear()
                        aux1 -= 1
                        aux2 += 1

                    instância = float((self.valorDeX.text()).replace(',', '.'))
                    produto = 1
                    sol = 0

                    for i, item in enumerate(seq):
                        sol += item[0] * produto
                        produto *= (instância - self.pontosX[i])
                        if i == 0:
                            sol = item[0]
                    sol = f'{sol:.{window.decimal}f}'
                    self.label3.setText(str(sol.replace('.', ',')))
                except:
                    message = QMessageBox(self)
                    message.setWindowTitle('Atenção!')
                    message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                    message.setText('Por favor, certifique-se de ter entrado\n'
                                    'com todos os dados necessários.')
                    message.setIcon(QMessageBox.Warning)
                    message.setStandardButtons(QMessageBox.Ok)
                    message.show()
                    self.valorDeX.clear()
                    self.numPontos.setFocus()
            else:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, primeiro\n'
                                'insira os pontos')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.valorDeX.clear()
                self.label3.clear()

    def plotar_grafico(self):
        if self.numPontos.text() != '':
            try:
                self.funcao()
                grafico_newton.show()
            except:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, certifique-se de ter entrado\n'
                                'com todos os dados necessários.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.numPontos.setFocus()
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, primeiro\n'
                            'insira os pontos')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            self.numPontos.setFocus()
            return 0

    def funcao(self):
        self.pontosX = list()
        self.pontosY = list()

        for i in range(0, len(inserir_pontos.xlist)):
            self.pontosX.append(float((inserir_pontos.xlist[i].text()).replace(',', '.')))
            self.pontosY.append(float((inserir_pontos.ylist[i].text()).replace(',', '.')))
        aux1 = len(self.pontosY) - 1
        aux2 = 1
        aux = list()
        seq = list()
        seq.append(self.pontosY[:])

        for c in range(0, len(self.pontosY) - 1):
            for i in range(0, aux1):
                derivada = (seq[c][i + 1] - seq[c][i]) / (self.pontosX[i + aux2] - self.pontosX[i])
                aux.append(derivada)
            seq.append(aux[:])
            aux.clear()
            aux1 -= 1
            aux2 += 1
        window.vetor1 = seq[:]
        window.vetor2 = self.pontosX[:]

    def refazer(self):
        self.numPontos.clear()
        self.valorDeX.clear()
        self.label3.clear()
        self.numPontos.setFocus()

    def validador_int_maior_que_um(self):
        if self.numPontos.text() != '':
            if len(self.numPontos.text()) > 1:
                if not self.numPontos.text()[-1] in '0123456789':
                    self.numPontos.setText(self.numPontos.text()[:-1])
            else:
                if not self.numPontos.text()[-1] in '123456789':
                    self.numPontos.setText(self.numPontos.text()[:-1])

    def validador_double(self):
        if self.valorDeX.text() != '':
            if len(self.valorDeX.text()) > 1:
                if not self.valorDeX.text()[-1] in ',.0123456789':
                    self.valorDeX.setText(self.valorDeX.text()[:-1])
                    return 0
            else:
                if not self.valorDeX.text()[-1] in '-0123456789':
                    self.valorDeX.setText(self.valorDeX.text()[:-1])
                    return 0


class MMQ(QWidget):
    def __init__(self):
        super().__init__()
        self.criar_janela_MMQ()

        self.vbox = QVBoxLayout()
        self.setLayout(self.vbox)

        self.botao_voltar_MMQ()
        self.textos_mmq()
        self.dialog_mmq()
        self.solução_mmq()

        #self.setFixedSize(window.width, window.height)

    def criar_janela_MMQ(self):
        self.titleMMQ = 'Curva - MMQ'
        left = 150
        top = 150
        width = 470
        height = 460

        #self.setGeometry(left, top, width, height)

    def botao_voltar_MMQ(self):
        buttonVoltar = QPushButton()
        buttonVoltar.setIcon(QtGui.QIcon('arquivos/voltar3.ico'))
        buttonVoltar.setIconSize(QtCore.QSize(90, 40))
        buttonVoltar.setMaximumSize(30, 30)
        buttonVoltar.clicked.connect(self.voltar_menu)
        self.vbox.addWidget(buttonVoltar)

    def textos_mmq(self):
        titulo = QLabel('Método dos mínimos quadrados')
        titulo.setFont(QtGui.QFont('Corbel Light', 13))
        titulo.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop)
        titulo.setMaximumHeight(80)

        self.vbox.addWidget(titulo)

    def dialog_mmq(self):
        grid1 = QGridLayout()

        grid2 = QGridLayout()

        vbox = QVBoxLayout()

        self.groupBox = QGroupBox(' Entrada de dados ')

        labelFormato = QLabel('Formato da equação:   ')

        labelImage = QLabel()
        pixmap = QPixmap('arquivos/formatoT.png')
        labelImage.setPixmap(pixmap)

        labelN = QLabel('n:  ')
        labelN.setFont(QtGui.QFont('Cambria Math', 11))
        labelN.setStyleSheet('color:green')

        self.n = QLineEdit()
        self.n.setFixedSize(60, 23)
        self.n.setFocus()
        self.n.textChanged.connect(self.validador_int_maior_que_um_n)

        buttonDefinirEquação = QPushButton('Definir equação')
        buttonDefinirEquação.setFixedSize(120, 25)
        buttonDefinirEquação.clicked.connect(self.definir_equacao)

        grid1.addWidget(labelFormato, 0, 0)
        grid1.addWidget(labelImage, 0, 1, QtCore.Qt.AlignLeft)
        grid1.setAlignment(QtCore.Qt.AlignLeft)

        label1 = QLabel('Número de pontos:  ')

        self.numPontos = QLineEdit()
        self.numPontos.setMinimumSize(20, 25)
        self.numPontos.setFocus()
        self.numPontos.setFixedSize(60, 23)
        self.numPontos.textChanged.connect(self.validador_int_maior_que_um)

        label2 = QLabel()
        label2.setMinimumSize(80, 25)

        inputPontos = QPushButton('Inserir pontos')
        inputPontos.setMinimumSize(120, 25)
        inputPontos.clicked.connect(self.inserir_pontos)

        grid2.addWidget(labelN, 0, 0, 1, 1, QtCore.Qt.AlignRight)
        grid2.addWidget(self.n, 0, 1, QtCore.Qt.AlignLeft)
        grid2.addWidget(buttonDefinirEquação, 0, 2, QtCore.Qt.AlignRight)

        grid2.addWidget(label1, 1, 0, QtCore.Qt.AlignRight)
        grid2.addWidget(self.numPontos, 1, 1,  QtCore.Qt.AlignLeft)
        grid2.addWidget(inputPontos, 1, 2, QtCore.Qt.AlignRight)

        vbox.addItem(grid1)
        vbox.addItem(grid2)
        self.groupBox.setLayout(vbox)

        self.vbox.addWidget(self.groupBox)

    def numero_coeficientes(self):
        return int(self.n.text())

    def definir_equacao(self):
        if self.n.text() != '':
            definir_eq.dados(self.numero_coeficientes())
            definir_eq.show()
            self.valorDeX.clear()
            self.label3.clear()
            self.numPontos.clear()
            self.numPontos.setFocus()
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, informe a quantidade de coeficientes.')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            self.n.setFocus()
            return 0

    def numero_pontos(self):
        return int(self.numPontos.text())

    def inserir_pontos(self):
        if self.numPontos.text() != '':
            if self.numero_pontos() < int(self.n.text()):
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('O número de pontos deve ser maior ou igual\n'
                                'ao número de coeficientes a resolver.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.numPontos.setFocus()
                return 0
            inserir_pontos.dados(self.numero_pontos())
            inserir_pontos.show()
            self.valorDeX.clear()
            self.label3.clear()
            self.valorDeX.setFocus()
        else:
            if self.n.text() == '':
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, primeiro defina a equação.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.n.setFocus()
                return 0
            else:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, informe o número de pontos.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.numPontos.setFocus()
                return 0

    def solução_mmq(self):
        grid1 = QGridLayout()

        grid2 = QGridLayout()
        grid2.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()

        self.group = QGroupBox(' Solução numérica ')
        self.group.setMaximumHeight(120)

        buttonExibirCoef = QPushButton('Exibr coeficientes')
        buttonExibirCoef.setFixedSize(120, 25)
        buttonExibirCoef.clicked.connect(self.exibirCoef)

        label1 = QLabel('        f (')
        label1.setMaximumWidth(40)

        self.valorDeX = QLineEdit()
        self.valorDeX.setMaximumSize(40, 20)
        self.valorDeX.textChanged.connect(self.mostrar_solucao)

        label2 = QLabel('  )  =')

        self.label3 = QLabel()
        self.label3.setFixedSize(120, 20)
        self.label3.setStyleSheet('background-color:rgb(255, 255, 255)')
        self.label3.setAlignment(QtCore.Qt.AlignCenter)

        label4 = QLabel()
        label4.setFixedSize(80, 100)

        buttonRefazer = QPushButton('Refazer')
        buttonRefazer.setFixedSize(120, 30)
        buttonRefazer.clicked.connect(self.refazer)

        buttonGrafico = QPushButton('Gerar gráfico')
        buttonGrafico.setFixedSize(150, 30)
        buttonGrafico.clicked.connect(self.plotar_grafico)

        grid1.addWidget(buttonExibirCoef, 0, 5)
        grid1.addWidget(label1, 2, 0)
        grid1.addWidget(self.valorDeX, 2, 1)
        grid1.addWidget(label2, 2, 2)
        grid1.addWidget(self.label3, 2, 3)
        grid1.addWidget(label4, 2, 4)
        grid2.addWidget(buttonGrafico, 0, 0)
        grid2.addWidget(buttonRefazer, 0, 1)

        vbox.addItem(grid1)
        self.group.setLayout(vbox)

        self.vbox.addWidget(self.group)
        self.vbox.addLayout(grid2)
        self.valorDeX.setFocus()

    def mostrar_solucao(self):
        if self.numPontos.text() != '':
            try:
                self.validador_double()
                termos = list()
                self.pontosX = list()
                self.pontosY = list()

                for i in range(0, len(inserir_pontos.xlist)):
                    self.pontosX.append(float((inserir_pontos.xlist[i].text()).replace(',', '.')))
                    self.pontosY.append(float((inserir_pontos.ylist[i].text()).replace(',', '.')))
                for i, eq in enumerate(definir_eq.fi_list):
                    aux = list()

                    if eq.currentText() == 'x^α':
                        tipo = 'exponenciação'
                        exp = float((definir_eq.aux_list[i].text()).replace(',', '.'))
                        aux.append(tipo)
                        aux.append(exp)
                        termos.append(aux[:])
                    elif eq.currentText() == 'e^x':
                        tipo = 'exponencial'
                        termos.append(tipo)
                    elif eq.currentText() == 'ln(x)':
                        tipo = 'log_neperiano'
                        termos.append(tipo)
                    elif eq.currentText() == 'logα(x)':
                        tipo = 'logaritmo'
                        base = float((definir_eq.aux_list[i].text()).replace(',', '.'))
                        aux.append(tipo)
                        aux.append(base)
                        termos.append(aux[:])

                matriz = formaçãoMatrizMMQ(self.pontosX, definir_eq.nCoef, termos)
                vetor = formaçãoVetorMMQ(self.pontosX, self.pontosY, definir_eq.nCoef, termos)
                triangularização(matriz, vetor)
                self.sol = retrosubstituição(matriz, vetor)

                window.vetor3 = termos[:]
                window.vetor4 = self.sol[:]

                if self.valorDeX.text() != '' and self.valorDeX.text() != '-':



                    instância = float((self.valorDeX.text()).replace(',', '.'))
                    resp = 0

                    for i in range(0, definir_eq.nCoef):
                        if 'exponenciação' in termos[i]:
                            fi = fun1(instância, termos[i][1])
                        elif 'exponencial' in termos[i]:
                            fi = fun2(instância)
                        elif 'log_neperiano' in termos[i]:
                            fi = fun3(instância)
                        elif 'logaritmo' in termos[i]:
                            fi = fun4(instância, termos[i][1])
                        resp += self.sol[i] * fi

                    resp = f'{resp:.{window.decimal}f}'
                    self.label3.setText(resp.replace('.', ','))
            except:
                if self.valorDeX.text() != '':
                    message = QMessageBox(self)
                    message.setWindowTitle('Atenção!')
                    message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                    message.setText('Por favor, certifique-se de ter entrado\n'
                                    'com todos os dados necessários.')
                    message.setIcon(QMessageBox.Warning)
                    message.setStandardButtons(QMessageBox.Ok)
                    message.show()
                    self.valorDeX.clear()
                    self.n.setFocus()
        else:
            pass

    def voltar_menu(self):
        w = Widget()
        window.setCentralWidget(w)
        window.setWindowTitle(window.MainTitle)

    def plotar_grafico(self):
        if self.numPontos.text() != '' and self.n.text() != '':
            try:
                self.funcao()
                grafico_mmq.show()
            except:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, certifique-se de ter entrado\n'
                                'com todos os dados necessários.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.n.setFocus()
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, primeiro\n'
                            'insira os dados')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            if self.n.text() == '':
                self.n.setFocus()
            else:
                self.numPontos.setFocus()
            return 0

    def funcao(self):
        termos = list()
        self.pontosX = list()
        self.pontosY = list()

        for i in range(0, len(inserir_pontos.xlist)):
            self.pontosX.append(float((inserir_pontos.xlist[i].text()).replace(',', '.')))
            self.pontosY.append(float((inserir_pontos.ylist[i].text()).replace(',', '.')))
        for i, eq in enumerate(definir_eq.fi_list):
            aux = list()

            if eq.currentText() == 'x^α':
                tipo = 'exponenciação'
                exp = float((definir_eq.aux_list[i].text()).replace(',', '.'))
                aux.append(tipo)
                aux.append(exp)
                termos.append(aux[:])
            elif eq.currentText() == 'e^x':
                tipo = 'exponencial'
                termos.append(tipo)
            elif eq.currentText() == 'ln(x)':
                tipo = 'log_neperiano'
                termos.append(tipo)
            elif eq.currentText() == 'logα(x)':
                tipo = 'logaritmo'
                base = float((definir_eq.aux_list[i].text()).replace(',', '.'))
                aux.append(tipo)
                aux.append(base)
                termos.append(aux[:])

        matriz = formaçãoMatrizMMQ(self.pontosX, definir_eq.nCoef, termos)
        vetor = formaçãoVetorMMQ(self.pontosX, self.pontosY, definir_eq.nCoef, termos)
        triangularização(matriz, vetor)
        self.sol = retrosubstituição(matriz, vetor)

        window.vetor3 = termos[:]
        window.vetor4 = self.sol[:]
        window.vetor5 = self.pontosX[:]
        window.vetor6 = self.pontosY[:]

    def refazer(self):
        self.n.clear()
        self.numPontos.clear()
        self.valorDeX.clear()
        self.label3.clear()
        self.n.setFocus()

    def exibirCoef(self):
        if self.n.text() == '' or self.numPontos.text() == '':
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Não há dados a serem mostrados')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
        else:
            try:
                self.mostrar_solucao()
                exibir_coef.show()
                exibir_coef.mostrar_coef(self.sol)
                self.valorDeX.setFocus()
            except:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, certifique-se de ter entrado\n'
                                'com todos os dados necessários.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                self.n.setFocus()

    def validador_int_maior_que_um(self):
        if self.numPontos.text() != '':
            if len(self.numPontos.text()) > 1:
                if not self.numPontos.text()[-1] in '0123456789':
                    self.numPontos.setText(self.numPontos.text()[:-1])
            else:
                if not self.numPontos.text()[-1] in '123456789':
                    self.numPontos.setText(self.numPontos.text()[:-1])

    def validador_int_maior_que_um_n(self):
        if self.n.text() != '':
            if len(self.n.text()) > 1:
                if not self.n.text()[-1] in '0123456789':
                    self.n.setText(self.n.text()[:-1])
            else:
                if not self.n.text()[-1] in '123456789':
                    self.n.setText(self.n.text()[:-1])

    def validador_double(self):
        if self.valorDeX.text() != '':
            if len(self.valorDeX.text()) > 1:
                if not self.valorDeX.text()[-1] in ',.0123456789':
                    self.valorDeX.setText(self.valorDeX.text()[:-1])
                    return 0
            else:
                if not self.valorDeX.text()[-1] in '-0123456789':
                    self.valorDeX.setText(self.valorDeX.text()[:-1])
                    return 0


class Inserir_pontos(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Entrada'
        left = 150 + window.left
        top = 150 + window.top
        width = 200
        height = 260
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.setLayout(self.vbox)
        self.cont = 0

    def dados(self, numPontos):
        self.cont +=1
        if self.cont > 1:
            self.vbox.itemAt(0).widget().setParent(None)
            self.vbox.itemAt(0).widget().setParent(None)
        self.nPontos = numPontos
        self.xlist = list()
        self.ylist = list()

        self.groupBox = QGroupBox()

        scroll = QScrollArea()

        form = QFormLayout()

        x = QLabel('x')
        y = QLabel('y')
        x.setMinimumSize(77, 20)
        y.setMinimumSize(77, 20)
        x.setAlignment(QtCore.Qt.AlignCenter)
        y.setAlignment(QtCore.Qt.AlignCenter)
        form.addRow(x, y)

        for i in range(0, self.nPontos):
            self.xlist.append(QLineEdit())
            self.ylist.append(QLineEdit())
            self.xlist[i].setMinimumSize(77, 20)
            self.xlist[i].setMaximumSize(77, 20)
            self.xlist[i].textChanged.connect(self.validador_double_x)
            self.ylist[i].setMinimumSize(77, 20)
            self.ylist[i].setMaximumSize(77, 20)
            self.ylist[i].textChanged.connect(self.validador_double_y)
            form.addRow(self.xlist[i], self.ylist[i])

        self.groupBox.setLayout(form)
        self.groupBox.setMaximumWidth(180)
        if self.nPontos > 6:
            scroll.setWidget(self.groupBox)
            scroll.setFixedSize(200, 180)
            self.vbox.addWidget(scroll)
        else:
            self.vbox.addWidget(self.groupBox)

        buttonFeito = QPushButton('Feito')
        buttonFeito.setMinimumSize(130, 28)
        buttonFeito.setMaximumSize(50, 25)
        buttonFeito.clicked.connect(self.feito)

        self.vbox.addWidget(buttonFeito)
        self.xlist[0].setFocus()

    def feito(self):
        # 1 - Verificação da entrada de dados
        for i in range(0, len(self.xlist)):
            if self.xlist[i].text() == '' or self.ylist[i].text() == '':
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, preencha\n'
                                'todas as células')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                if self.xlist[i].text() == '':
                    self.xlist[i].setFocus()
                else:
                    self.ylist[i].setFocus()
                return 0

        for i, lineEdit1 in enumerate(self.xlist):
            for j, lineEdit2 in enumerate(self.xlist):
                if i != j:
                    if lineEdit1.text() == lineEdit2.text():
                        # Janela Pupup
                        message = QMessageBox(self)
                        message.setWindowTitle('Atenção!')
                        message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                        message.setText('Por favor, não repita\n'
                                        'o valor de x')
                        message.setIcon(QMessageBox.Warning)
                        message.setStandardButtons(QMessageBox.Ok)
                        message.show()
                        return 0

        self.hide()

    def validador_double_x(self):
        for i in range(0, len(self.xlist)):
            if self.xlist[i].text() != '':
                if len(self.xlist[i].text()) > 1:
                    if not self.xlist[i].text()[-1] in ',.0123456789':
                        self.xlist[i].setText(self.xlist[i].text()[:-1])
                else:
                    if not self.xlist[i].text()[-1] in '-0123456789':
                        self.xlist[i].setText(self.xlist[i].text()[:-1])

    def validador_double_y(self):
        for i in range(0, len(self.ylist)):
            if self.ylist[i].text() != '':
                if len(self.ylist[i].text()) > 1:
                    if not self.ylist[i].text()[-1] in ',.0123456789':
                        self.ylist[i].setText(self.ylist[i].text()[:-1])
                else:
                    if not self.ylist[i].text()[-1] in '-0123456789':
                        self.ylist[i].setText(self.ylist[i].text()[:-1])


class Definir_equação(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Entrada'
        left = 150 + window.left
        top = 150 + window.top
        width = 400
        height = 180
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal) # Impede o retorno para a MainWindow sem fechar a QDialog

        self.setLayout(self.vbox)
        self.cont = 0

    def dados(self, numCoeficientes):
        self.cont += 1
        if self.cont > 1:
            self.vbox.itemAt(0).widget().setParent(None)
            self.vbox.itemAt(0).layout().setParent(None)
            self.buttonFeito.setParent(None)
        self.nCoef = numCoeficientes
        self.aux_list = list()
        self.aux2_list = list()
        self.fi_list = list()

        self.groupBox = QGroupBox()
        self.groupBox.setMaximumHeight(125)
        vbox = QVBoxLayout()

        scroll = QScrollArea()

        grid = QGridLayout()
        grid.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)

        f_deX = QLabel('f(x) = ')
        f_deX.setMinimumSize(40, 20)

        grid.addWidget(f_deX, 0, 0)

        funções = ['','x^α', 'e^x', 'ln(x)', 'logα(x)']
        CONT = 1

        for i in range(0, self.nCoef):
            alfa = QLabel(' Alfa = ')
            if i == 0:
                coeficiente = QLabel(f'a{i}')
                coeficiente.setMinimumWidth(30)
            else:
                coeficiente = QLabel(f' + a{i} ')
                coeficiente.setMinimumWidth(40)
            coeficiente.setFont(QtGui.QFont('Cambria Math', 11))
            self.fi_list.append(QComboBox())
            self.fi_list[i].addItems(funções)
            self.aux_list.append(QLineEdit())
            self.aux_list[i].setMaximumWidth(40)
            self.aux_list[i].textChanged.connect(self.validador_double)
            self.aux_list[i].hide()
            self.aux2_list.append(alfa)
            self.aux2_list[i].hide()
            grid.addWidget(coeficiente, 0, CONT, QtCore.Qt.AlignCenter)
            grid.addWidget(self.fi_list[i], 0, CONT + 1, QtCore.Qt.AlignCenter)
            grid.addWidget(self.aux2_list[i], 1, CONT, QtCore.Qt.AlignCenter)
            grid.addWidget(self.aux_list[i], 1, CONT + 1, QtCore.Qt.AlignCenter)
            self.fi_list[i].currentTextChanged.connect(self.mostrar_alfa)

            CONT += 2

        vbox.addItem(grid)
        self.groupBox.setLayout(vbox)

        if self.nCoef > 3:
            scroll.setWidget(self.groupBox)
            scroll.setFixedSize(380, 145)
            self.vbox.addWidget(scroll)
        else:
            self.vbox.addWidget(self.groupBox)

        self.buttonFeito = QPushButton('Feito')
        self.buttonFeito.setMinimumSize(130, 28)
        self.buttonFeito.setMaximumSize(50, 25)
        self.buttonFeito.clicked.connect(self.feito)

        self.grid2 = QGridLayout()
        self.grid2.setAlignment(QtCore.Qt.AlignRight)
        self.grid2.addWidget(self.buttonFeito, 0, 0)

        self.vbox.addLayout(self.grid2)

    def mostrar_alfa(self):
        for i, combo in enumerate(self.fi_list):
            if combo.currentText() == 'x^α' or combo.currentText() == 'logα(x)':
                self.aux_list[i].show()
                self.aux2_list[i].show()
            else:
                if not self.aux_list[i].isHidden():
                    self.aux_list[i].hide()
                    self.aux2_list[i].hide()

    def feito(self):
        # 1 - Verificação da entrada de dados
        for i in range(0, len(self.fi_list)):
            if self.fi_list[i].currentText() == '':
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('Por favor, preencha\n'
                                'todas as células')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                return 0
            if not self.aux_list[i].isHidden():
                if self.aux_list[i].text() == '':
                    message = QMessageBox(self)
                    message.setWindowTitle('Atenção!')
                    message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                    message.setText('Por favor, preencha\n'
                                    'todas as células')
                    message.setIcon(QMessageBox.Warning)
                    message.setStandardButtons(QMessageBox.Ok)
                    message.show()
                    return 0
        for i in range(0, len(self.fi_list)):
            for j in range(0, len(self.fi_list)):
                if j != i:
                    if self.fi_list[j].currentText() == self.fi_list[i].currentText():
                        if self.fi_list[j].currentText() == 'x^α':
                            if self.aux_list[j].text() == self.aux_list[i].text():
                                message = QMessageBox(self)
                                message.setWindowTitle('Atenção!')
                                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                                message.setText('Por favor, não repita a função.')
                                message.setIcon(QMessageBox.Warning)
                                message.setStandardButtons(QMessageBox.Ok)
                                message.show()
                                return 0
                        else:
                            message = QMessageBox(self)
                            message.setWindowTitle('Atenção!')
                            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                            message.setText('Por favor, não repita a função.')
                            message.setIcon(QMessageBox.Warning)
                            message.setStandardButtons(QMessageBox.Ok)
                            message.show()
                            return 0
        self.hide()

    def validador_double(self):
        for i in range(0, len(self.aux_list)):
            if self.aux_list[i].text() != '':
                if len(self.aux_list[i].text()) > 1:
                    if not self.aux_list[i].text()[-1] in ',.0123456789':
                        self.aux_list[i].setText(self.aux_list[i].text()[:-1])
                else:
                    if not self.aux_list[i].text()[-1] in '-0123456789':
                        self.aux_list[i].setText(self.aux_list[i].text()[:-1])


class Exibir_coef(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Saída'
        left = 150 + window.left
        top = 150 + window.top
        width = 180
        height = 200
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setLayout(self.vbox)
        self.cont = 0

    def mostrar_coef(self, sol):
        self.cont += 1
        if self.cont > 1:
            self.vbox.itemAt(0).widget().setParent(None)

        form = QFormLayout()

        GroupBox = QGroupBox('  Coeficientes  ')

        Scroll = QScrollArea()
        for i, coef in enumerate(sol):
            label1 = QLabel(f'a{i} :  ')
            resultado = f'{sol[i]:.{window.decimal}f}'
            label2 = QLabel(resultado.replace('.', ','))
            form.addRow(label1, label2)
        GroupBox.setLayout(form)
        GroupBox.setMinimumWidth(140)
        if len(sol) > 9:
            Scroll.setWidget(GroupBox)
            Scroll.setFixedSize(170, 180)
            self.vbox.addWidget(Scroll)
        else:
            self.vbox.addWidget(GroupBox)


class Casas_decimais(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Configuração - Casas decimais'
        left = 150 + window.left
        top = 150 + window.top
        width = 200
        height = 100
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.group = QGroupBox('Escolha uma opção')

        grid = QGridLayout()
        grid.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()
        vbox.addWidget(self.group)

        self.setLayout(vbox)

        hbox = QHBoxLayout()

        self.uma = QRadioButton('Uma')
        self.duas = QRadioButton('Duas')
        self.tres = QRadioButton('Três')
        self.quatro = QRadioButton('Quatro')
        self.cinco = (QRadioButton('Cinco'))
        hbox.addWidget(self.uma)
        hbox.addWidget(self.duas)
        hbox.addWidget(self.tres)
        hbox.addWidget(self.quatro)
        hbox.addWidget(self.cinco)

        self.group.setLayout(hbox)

        buttonAplicar = QPushButton('Aplicar')
        buttonAplicar.clicked.connect(window.aplicar_decimal)
        grid.addWidget(buttonAplicar, 0, 0)

        vbox.addLayout(grid)


class Conf_grafico(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Configuração - Gráfico'
        left = 150 + window.left
        top = 150 + window.top
        width = 250
        height = 250
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.temas = ['Solarize_Light2', 'bmh', 'classic', 'dark_background', 'ggplot', 'grayscale', 'seaborn', 'seaborn-dark', 'seaborn-ticks', 'seaborn-white', 'seaborn-whitegrid']
        self.cores = ['Azul', 'Branco', 'Verde', 'Vermelho', 'Amarelo', 'Laranja', 'Marrom', 'Preto']
        self.colors = ['blue', 'white', 'green', 'red', 'yellow', 'orange', 'brown', 'black']
        self.tFonte = ['7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']

        vbox = QVBoxLayout()

        group1 = QGroupBox(' Temas ')
        group2 = QGroupBox(' Cores / Tamanho da fonte ')

        self.grid1 = QGridLayout()

        grid2 = QGridLayout()

        grid3 = QGridLayout()
        grid3.setAlignment(QtCore.Qt.AlignRight)

        labelTemas = QLabel('Tema: ')
        self.boxTemas = QComboBox()
        self.boxTemas.currentTextChanged.connect(self.mudar_tema)
        self.boxTemas.addItems(self.temas)

        self.labelImage = QLabel()
        #self.pixmap = QPixmap('tema0.png')
        #self.labelImage.setPixmap(self.pixmap)

        labelTitulo = QLabel('Título: ')
        self.boxTitulo1 = QComboBox()
        self.boxTitulo1.addItems(self.cores)
        self.boxTitulo2 = QComboBox()
        self.boxTitulo2.addItems(self.tFonte)

        labelx = QLabel('Legenda em x: ')
        self.boxx1 = QComboBox()
        self.boxx1.addItems(self.cores)
        self.boxx2 = QComboBox()
        self.boxx2.addItems(self.tFonte)

        labely = QLabel('Legenda em y: ')
        self.boxy1 = QComboBox()
        self.boxy1.addItems(self.cores)
        self.boxy2 = QComboBox()
        self.boxy2.addItems(self.tFonte)

        buttonAplicar = QPushButton('Aplicar')
        buttonAplicar.clicked.connect(window.aplicar_conf_grafico)

        self.grid1.addWidget(labelTemas, 0, 0, QtCore.Qt.AlignRight)
        self.grid1.addWidget(self.boxTemas, 0, 1, QtCore.Qt.AlignLeft)
        #self.grid1.addWidget(self.labelImage, 1, 0, 1, 2, QtCore.Qt.AlignCenter)

        grid2.addWidget(labelTitulo, 0, 0)
        grid2.addWidget(self.boxTitulo1, 0, 1)
        grid2.addWidget(self.boxTitulo2, 0, 2)
        grid2.addWidget(labelx, 1, 0)
        grid2.addWidget(self.boxx1, 1, 1)
        grid2.addWidget(self.boxx2, 1, 2)
        grid2.addWidget(labely, 2, 0)
        grid2.addWidget(self.boxy1, 2, 1)
        grid2.addWidget(self.boxy2, 2, 2)

        grid3.addWidget(buttonAplicar, 0, 0)

        group1.setLayout(self.grid1)
        group2.setLayout(grid2)

        vbox.addWidget(group1)
        vbox.addWidget(group2)
        vbox.addLayout(grid3)
        self.setLayout(vbox)

    def mudar_tema(self):
        for i, tema in enumerate(self.temas):
            if self.boxTemas.currentText() == tema:
                pixmap = QPixmap(f'arquivos/tema{i}.png')
                self.labelImage = QLabel()
                self.labelImage.setPixmap(pixmap)
                self.labelImage.setFixedSize(218, 150)
                self.grid1.addWidget(self.labelImage, 1, 0, 1, 2, QtCore.Qt.AlignCenter)


class Grafico_newton(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Gráfico Newton'
        left = 150 + window.left
        top = 150 + window.top
        width = 150
        height = 200
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.grid = QGridLayout()

        self.LabelTitulo = QLabel('Título: ')

        self.titulo = QLineEdit()
        self.titulo.setFixedSize(120, 20)

        self.LabellegendaX = QLabel('Legenda de x: ')

        self.legendaX = QLineEdit()
        self.legendaX.setFixedSize(120, 20)

        self.LabellegendaY = QLabel('Legenda de y: ')

        self.legendaY = QLineEdit()
        self.legendaY.setFixedSize(120, 20)

        self.cores = ['Azul', 'Branco', 'Verde', 'Vermelho', 'Amarelo', 'Laranja', 'Marrom', 'Preto']
        self.colors = ['blue', 'white', 'green', 'red', 'yellow', 'orange', 'brown', 'black']

        self.LabelCor = QLabel('Cor da curva: ')

        self.cor = QComboBox()
        self.cor.addItems(self.cores)

        self.labelIntervalo = QLabel('Intervalo: ')
        self.inf = QLineEdit()
        self.inf.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.inf.setFixedSize(50, 20)

        self.label = QLabel(' : ')
        self.label.setFixedSize(6, 20)

        self.sup = QLineEdit()
        self.sup.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.sup.setFixedSize(50, 20)

        self.buttonGerarGrafico = QPushButton('Gerar gráfico')
        self.buttonGerarGrafico.setFixedSize(90, 30)
        self.buttonGerarGrafico.clicked.connect(self.gerar_grafico)

        self.grid.addWidget(self.LabelTitulo, 0, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.titulo, 0, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabellegendaX, 1, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendaX, 1, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabellegendaY, 2, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendaY, 2, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.labelIntervalo, 3, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.inf, 3, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.label, 3, 2, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.sup, 3, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabelCor, 4, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.cor, 4, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.buttonGerarGrafico, 5, 1, 1, 3, QtCore.Qt.AlignCenter)

        self.setLayout(self.grid)

    def gerar_grafico(self):
        for i, cor in enumerate(self.cores):
            if self.cor.currentText() == cor:
                color = self.colors[i]
        if self.inf.text() != '' and self.sup.text() != '':
            if self.inf.text() < self.sup.text():
                grafico_newton.hide()
                plotar_grafico_simples(self.legendaX.text(), self.legendaY.text(), self.titulo.text(),
                                       float(self.inf.text().replace(',', '.')), float(self.sup.text().replace(',', '.')), color)
            else:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('O segundo valor do intervalo deve\n'
                                'ser maior que o primeiro.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                return 0
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, informe pelo menos o intervalo.')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            return 0


class Grafico_MMQ(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Gráfico MMQ'
        left = 150 + window.left
        top = 150 + window.top
        width = 200
        height = 100
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.grid = QGridLayout()

        self.LabelTitulo = QLabel('Título: ')

        self.titulo = QLineEdit()
        self.titulo.setFixedSize(120, 20)

        self.LabellegendaX = QLabel('Legenda de x: ')

        self.legendaX = QLineEdit()
        self.legendaX.setFixedSize(120, 20)

        self.LabellegendaY = QLabel('Legenda de y: ')

        self.legendaY = QLineEdit()
        self.legendaY.setFixedSize(120, 20)

        self.cores = ['Azul', 'Branco', 'Verde', 'Vermelho', 'Amarelo', 'Laranja', 'Marrom', 'Preto']
        self.colors = ['blue', 'white', 'green', 'red', 'yellow', 'orange', 'brown', 'black']

        self.LabelCor_grafico = QLabel('Cor da curva: ')

        self.cor_grafico = QComboBox()
        self.cor_grafico.addItems(self.cores)

        self.LabelCor_pontos = QLabel('Cor dos pontos: ')

        self.cor_pontos = QComboBox()
        self.cor_pontos.addItems(self.cores)

        self.labelIntervalo = QLabel('Intervalo: ')
        self.inf = QLineEdit()
        self.inf.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.inf.setFixedSize(50, 20)

        self.label = QLabel(' : ')
        self.label.setFixedSize(6, 20)

        self.sup = QLineEdit()
        self.sup.setValidator(QDoubleValidator(-9999.00, 9999.00, 3))
        self.sup.setFixedSize(50, 20)

        self.buttonGerarGrafico = QPushButton('Gerar gráfico')
        self.buttonGerarGrafico.setFixedSize(90, 30)
        self.buttonGerarGrafico.clicked.connect(self.gerar_grafico)

        self.grid.addWidget(self.LabelTitulo, 0, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.titulo, 0, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabellegendaX, 1, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendaX, 1, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabellegendaY, 2, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.legendaY, 2, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.labelIntervalo, 3, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.inf, 3, 1, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.label, 3, 2, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.sup, 3, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabelCor_grafico, 4, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.cor_grafico, 4, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.LabelCor_pontos, 5, 0, QtCore.Qt.AlignRight)
        self.grid.addWidget(self.cor_pontos, 5, 1, 1, 3, QtCore.Qt.AlignLeft)
        self.grid.addWidget(self.buttonGerarGrafico, 6, 1, 1, 3, QtCore.Qt.AlignCenter)

        self.setLayout(self.grid)

    def gerar_grafico(self):
        for i, cor in enumerate(self.cores):
            if self.cor_grafico.currentText() == cor:
                color_grafico = self.colors[i]
            if self.cor_pontos.currentText() == cor:
                color_pontos = self.colors[i]
        if self.inf.text() != '' and self.sup.text() != '':
            if float(self.inf.text()) < float(self.sup.text()):
                grafico_mmq.hide()
                plotar_grafico_mmq(self.legendaX.text(), self.legendaY.text(), self.titulo.text(),
                                    float(self.inf.text().replace(',', '.')), float(self.sup.text().replace(',', '.')), color_grafico, color_pontos)
            else:
                message = QMessageBox(self)
                message.setWindowTitle('Atenção!')
                message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
                message.setText('O segundo valor do intervalo deve\n'
                                'ser maior que o primeiro.')
                message.setIcon(QMessageBox.Warning)
                message.setStandardButtons(QMessageBox.Ok)
                message.show()
                return 0
        else:
            message = QMessageBox(self)
            message.setWindowTitle('Atenção!')
            message.setWindowIcon(QtGui.QIcon('arquivos/icone.ico'))
            message.setText('Por favor, informe pelo menos o intervalo.')
            message.setIcon(QMessageBox.Warning)
            message.setStandardButtons(QMessageBox.Ok)
            message.show()
            return 0


class suporte(QDialog):
    def __init__(self):
        super().__init__()
        title = 'Suporte'
        left = 150 + window.left
        top = 150 + window.top
        width = 200
        height = 80
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        vbox =QVBoxLayout()
        label = QLabel('Envie sua dúvida para: rafa10031999@gmail.com')
        vbox.addWidget(label)

        self.setLayout(vbox)


def plotar_grafico_simples(legendaX, legendaY, titulo, inf, sup, cor_do_grafico='blue'):
    x1 = np.arange(inf, sup + 0.01, 0.01)

    sols = list()

    for valor in x1:
        produto = 1
        sol = 0
        for i, item in enumerate(window.vetor1):
            sol += item[0] * produto
            produto *= (valor - window.vetor2[i])
            if i == 0:
                sol = item[0]
        sols.append(sol)
    

    plt.style.use(window.tema)
    plt.plot(x1, sols, color=cor_do_grafico)
    plt.title(titulo, color=window.cortitulo, fontsize=window.fontsizetitle)
    plt.xlabel(legendaX, color=window.corx, fontsize=window.fontsizex)
    plt.ylabel(legendaY, color=window.cory, fontsize=window.fontsizey)
    fig = plt.gcf()
    fig.canvas.set_window_title('Gráfico')
    plt.show()


def plotar_grafico_mmq(legendaX, legendaY, titulo, inf, sup, cor_do_grafico='blue', cor_dos_pontos='red'):
    x1 = np.arange(inf, sup + 0.01, 0.01)
    x2 = window.vetor5
    y2 = window.vetor6
    cont = 0
    for i in range(0, len(x2)):
        if x2[cont] > sup or x2[cont] < inf:
            del x2[cont]
            del y2[cont]
        else:
            cont += 1
    sols = list()

    for valor in x1:
        resp = 0

        for i in range(0, definir_eq.nCoef):
            if 'exponenciação' in window.vetor3[i]:
                fi = fun1(valor, window.vetor3[i][1])
            elif 'exponencial' in window.vetor3[i]:
                fi = fun2(valor)
            elif 'log_neperiano' in window.vetor3[i]:
                fi = fun3(valor)
            elif 'logaritmo' in window.vetor3[i]:
                fi = fun4(valor, window.vetor3[i][1])
            resp += window.vetor4[i] * fi
        sols.append(resp)

    plt.style.use(window.tema)
    plt.plot(x1, sols, color=cor_do_grafico)
    plt.scatter(x2, y2, color=cor_dos_pontos)
    plt.title(titulo, color=window.cortitulo, fontsize=window.fontsizetitle)
    plt.xlabel(legendaX, color=window.corx, fontsize=window.fontsizex)
    plt.ylabel(legendaY, color=window.cory, fontsize=window.fontsizey)
    plt.show()


DEFAULT_STYLE = """
QProgressBar{
    border: 0px solid grey;
    border-radius: 0px;
    text-align: center
}

QProgressBar::chunk {
    background-color: lightblue;
    width: 0.5px;
    margin: 0px;
}
"""

App = QApplication(sys.argv)
App.setStyle('Fusion')
spla = QSplashScreen(QPixmap('arquivos/Splash2.jpg'))
barra = QLabel(spla)
barra.setGeometry(500, 10, 200, 2)
barra.setStyleSheet('background-color:lightblue')
icone = QPixmap('arquivos/logo8.png')
LabelIcone = QLabel(spla)
LabelIcone.setStyleSheet('background-color:white')
LabelIcone.setPixmap(icone)
LabelIcone.setGeometry(0, 20, 370, 111)
LabelIcone.setAlignment(QtCore.Qt.AlignCenter)
versao = QLabel(spla)
versao.setText('Versão 1.0, Março de 2020')
versao.setGeometry(15, 450, 150, 20)

Autor = QLabel(spla)
Autor.setText('R. Vieira')
Autor.setFont(QtGui.QFont('Bookman Old Style', 10))
Autor.setGeometry(600, 450, 100, 20)

progressBar = QProgressBar(spla)
progressBar.setGeometry(0, 430, 692, 5)
progressBar.setTextVisible(False)
progressBar.setStyleSheet(DEFAULT_STYLE)

spla.show()
for i in range(0, 100):pipi
    progressBar.setValue(i)
    t = time.time()
    while time.time() < t + 0.03:
        App.processEvents()


widget = Widget()
window = Window()

pygame.init()
pygame.mixer.music.load('arquivos/initialization.mp3')
pygame.mixer.music.set_volume(0.5)
pygame.mixer.music.play()

spla.finish(window)
newton = Newton()
mmq = MMQ()
inserir_pontos = Inserir_pontos()
definir_eq = Definir_equação()
exibir_coef = Exibir_coef()
decimal = Casas_decimais()
conf_grafico = Conf_grafico()
grafico_newton = Grafico_newton()
grafico_mmq = Grafico_MMQ()
suporte = suporte()
sys.exit(App.exec_())
