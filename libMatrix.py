from math import *


def triangularization(matriz, vetor):
    for col in range(0, len(matriz) - 1):
        for linha in range(col + 1, len(matriz)):
            zeroElement(matriz, linha, col, vetor)

def zeroElement(matriz , lin, col, vetor):
    b = - matriz[lin][col] / matriz[col][col]
    matriz[lin][col] = 0

    for c in range(col + 1, len(matriz)):
        matriz[lin][c] += b * matriz[col][c]
    vetor[lin] += b * vetor[col]

def retrosubstitution(matriz, vetor):
    sol1 = list()
    sol2 = list()
    a = len(matriz) - 1
    for i in range(a, -1, -1):
        soma = 0
        if i != a:
            for j in range(i + 1, a + 1):
                soma += matriz[i][j] * sol1[a - j]
        sol1.append((vetor[i] - soma) / matriz[i][i])
    for i in range(len(sol1) - 1, -1, -1):
        sol2.append(sol1[i])
    return sol2

def fun1(x, alfa):
    return x ** alfa

def fun2(x):
    return exp(x)

def fun3(x):
    return log(x, exp(1))

def fun4(x, alfa):
    return log(x, alfa)

def makeMatrixMMQ(_x, _n, _termos):
    matriz = list()
    linha = list()
    for i in range(0, _n):
        for j in range(0, _n):
            soma = 0
            a = _termos[i]
            b = _termos[j]
            for valor in _x:
                if 'exponenciação' in a:
                    fi_i = fun1(valor, a[1])
                elif 'exponencial' in a:
                    fi_i = fun2(valor)
                elif 'log_neperiano' in a:
                    fi_i = fun3(valor)
                elif 'logaritmo' in a:
                    fi_i = fun4(valor, a[1])
                if 'exponenciação' in b:
                    fi_j = fun1(valor, b[1])
                elif 'exponencial' in b:
                    fi_j = fun2(valor)
                elif 'log_neperiano' in b:
                    fi_j = fun3(valor)
                elif 'logaritmo' in b:
                    fi_j = fun4(valor, b[1])
                soma += fi_i * fi_j
            linha.append(soma)
        matriz.append(linha[:])
        linha.clear()
    return matriz[:]

def makeVectorMMQ(x, y, n, termos):
    col = list()
    for j in range(0, n):
        soma = 0
        a = termos[j]
        for i, valor in enumerate(x):
            if 'exponenciação' in a:
                fi_i = fun1(valor, a[1])
            elif 'exponencial' in a:
                fi_i = fun2(valor)
            elif 'log_neperiano' in a:
                fi_i = fun3(valor)
            elif 'logaritmo' in a:
                fi_i = fun4(valor, a[1])

            soma += y[i] * fi_i
        col.append(soma)
    return col[:]
