from PyQt5.QtWidgets import QStackedWidget
from myWidget import MyWidget
from NewtonWidget import Newton
from MMQWidget import MMQ


class MyStackedWidget(QStackedWidget):
    def __init__(self):
        super(MyStackedWidget, self).__init__()

        self.myWidget = MyWidget()
        self.newton = Newton()
        self.mmq = MMQ()

        self.addWidget(self.myWidget)
        self.addWidget(self.newton)
        self.addWidget(self.mmq)

        self.myWidget.goToNewton_button.clicked.connect(self.go_to_newton)
        self.myWidget.goToMMQ_button.clicked.connect(self.go_to_mmq)
        self.newton.goBack_button.clicked.connect(self.go_to_myWidget)
        self.mmq.goBack_button.clicked.connect(self.go_to_myWidget)
    
    def go_to_myWidget(self):
        self.setCurrentWidget(self.myWidget)
        self.myWindow.setWindowTitle(self.myWindow.mainTitle)

    def go_to_newton(self):
        self.setCurrentWidget(self.newton)
        self.myWindow.setWindowTitle(self.newton.title)
        self.newton.numPontos_lineEdit.setFocus()

    def go_to_mmq(self):
        self.setCurrentWidget(self.mmq)
        self.myWindow.setWindowTitle(self.mmq.title)
        self.mmq.n_lineEdit.setFocus()

    def set_myWindow(self, _myWindow):
        self.myWindow = _myWindow

        #send myWindow to newton and mmq
        self.newton.setMyWindow(self.myWindow)
        self.mmq.setMyWindow(self.myWindow)