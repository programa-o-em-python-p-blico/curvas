from PyQt5.QtWidgets import QDialog, QVBoxLayout, QGroupBox, QScrollArea, QFormLayout, QLabel, QLineEdit, QPushButton, QGridLayout, QComboBox
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import QDoubleValidator
from myMessgeBox_Warning import MyMessageBox
import time
import pandas as pd



class InsertPoints(QDialog):
    def __init__(self, _numPontos, _decimals, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Entrada'
        left = 650
        top = 250
        width = 200
        height = 260
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        self.setLayout(self.vbox)
      
        self.decimals = _decimals
        self.nPontos = _numPontos
        self.xlist = list()
        self.ylist = list()

        self.groupBox = QGroupBox()

        self.scroll = QScrollArea()

        self.form = QFormLayout()

        self.x_label = QLabel('x')
        self.y_label = QLabel('y')
        self.x_label.setMinimumSize(77, 20)
        self.y_label.setMinimumSize(77, 20)
        self.x_label.setAlignment(QtCore.Qt.AlignCenter)
        self.y_label.setAlignment(QtCore.Qt.AlignCenter)
        self.form.addRow(self.x_label, self.y_label)

        for i in range(0, self.nPontos):
            self.xlist.append(QLineEdit())
            self.ylist.append(QLineEdit())
            self.xlist[i].setMinimumSize(77, 20)
            self.xlist[i].setMaximumSize(77, 20)
            self.xlist[i].setValidator(QDoubleValidator(9999999, -9999999, self.decimals))
            #self.xlist[i].textChanged.connect(self.validador_float)
            self.ylist[i].setMinimumSize(77, 20)
            self.ylist[i].setMaximumSize(77, 20)
            self.ylist[i].setValidator(QDoubleValidator(9999999, -9999999, self.decimals))
            #self.ylist[i].textChanged.connect(self.validador_float)
            self.form.addRow(self.xlist[i], self.ylist[i])

        self.groupBox.setLayout(self.form)
        self.groupBox.setMaximumWidth(180)
        if self.nPontos > 6:
            self.scroll.setWidget(self.groupBox)
            self.scroll.setFixedSize(200, 180)
            self.vbox.addWidget(self.scroll)
        else:
            self.vbox.addWidget(self.groupBox)

        self.done_button = QPushButton('Feito')
        self.done_button.setMinimumSize(130, 28)
        self.done_button.setMaximumSize(50, 25)
        self.done_button.clicked.connect(self.done)

        self.vbox.addWidget(self.done_button)
        self.xlist[0].setFocus()
    
    def closeEvent(self, event):
        pass

    def done(self):
        for i in range(0, len(self.xlist)):
            if self.xlist[i].text() == '' or self.ylist[i].text() == '':
                
                # Message Box
                message = MyMessageBox('Atenção', 'Por favor, preencha\ntodas as células.', self)
                message.show()

                if self.xlist[i].text() == '':
                    self.xlist[i].setFocus()
                else:
                    self.ylist[i].setFocus()
                
                # GAMBIARRA PARA ENTRAR COM OS PONTOS DO EXCEL
                
                data = pd.read_excel('D:/Documentos/UFF/Estudos orientados/Análise dinâmica de estruturas/Projeto da passarela/Pultrudado/autovetor_modo2.xlsx')
                # for i, x in enumerate(data['x(m)']):
                for i in range(self.nPontos):
                    x = str(data['x(m)'][i]).replace(',', '.')
                    x = float(x)
                    x = f'{x:.4f}'
                    fi = str(data['Φ'][i])
                    fi = float(fi)
                    fi = f'{fi:.7f}'
                    self.xlist[i].setText(x)
                    self.ylist[i].setText(fi)

                return 0

        for i, lineEdit1 in enumerate(self.xlist):
            for j, lineEdit2 in enumerate(self.xlist):
                if i != j:
                    if lineEdit1.text() == lineEdit2.text():
                        
                        # Message Box
                        message = MyMessageBox('Atenção', 'Por favor, não repita\no valor de x.', self)
                        message.show()
                        return 0
        


        xlist = list()
        ylist = list()
        for i in range(0, len(self.xlist)):
            xlist.append(float(self.xlist[i].text().replace(',', '.')))
            ylist.append(float(self.ylist[i].text().replace(',', '.')))

        self.currentWidget.setPoints(xlist, ylist)
        self.currentWidget.valorDeX_lineEdit.clear()
        self.currentWidget.solution_label.clear()
        self.currentWidget.valorDeX_lineEdit.setFocus()
        self.setParent(None)
    
    def setCurrentWidget(self, _currentWidget):
        self.currentWidget = _currentWidget


class DefineEquation(QDialog):
    def __init__(self, _numCoeficientes, _decimals, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Entrada'
        left = 650 
        top = 250 
        width = 400
        height = 180
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)
        self.vbox = QVBoxLayout()

        self.setWindowModality(QtCore.Qt.ApplicationModal) # Impede o retorno para a MainWindow sem fechar a QDialog

        self.setLayout(self.vbox)

        self.decimals = _decimals
        self.nCoef = _numCoeficientes
        self.constant_list = list()
        self.aux_list = list()
        self.fi_list = list()

        self.groupBox = QGroupBox()
        self.groupBox.setMaximumHeight(125)
        vbox = QVBoxLayout()

        scroll = QScrollArea()

        grid = QGridLayout()
        grid.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)

        f_deX = QLabel('f(x) = ')
        f_deX.setMinimumSize(40, 20)

        grid.addWidget(f_deX, 0, 0)

        funções = ['','x^α', 'e^x', 'ln(x)', 'logα(x)']
        CONT = 1

        for i in range(0, self.nCoef):
            alfa = QLabel(' Alfa = ')
            if i == 0:
                coeficiente = QLabel(f'a{i}')
                coeficiente.setMinimumWidth(30)
            else:
                coeficiente = QLabel(f' + a{i} ')
                coeficiente.setMinimumWidth(40)
            coeficiente.setFont(QtGui.QFont('Cambria Math', 11))
            self.fi_list.append(QComboBox())
            self.fi_list[i].addItems(funções)
            self.constant_list.append(QLineEdit())
            self.constant_list[i].setMaximumWidth(40)
            self.constant_list[i].setValidator(QDoubleValidator(9999, -9999, self.decimals ))
            #self.aux_list[i].textChanged.connect(self.validador_float)
            self.constant_list[i].hide()
            self.aux_list.append(alfa)
            self.aux_list[i].hide()
            grid.addWidget(coeficiente, 0, CONT, QtCore.Qt.AlignCenter)
            grid.addWidget(self.fi_list[i], 0, CONT + 1, QtCore.Qt.AlignCenter)
            grid.addWidget(self.aux_list[i], 1, CONT, QtCore.Qt.AlignCenter)
            grid.addWidget(self.constant_list[i], 1, CONT + 1, QtCore.Qt.AlignCenter)
            self.fi_list[i].currentTextChanged.connect(self.mostrar_alfa)

            CONT += 2

        vbox.addItem(grid)
        self.groupBox.setLayout(vbox)

        if self.nCoef > 3:
            scroll.setWidget(self.groupBox)
            scroll.setFixedSize(380, 145)
            self.vbox.addWidget(scroll)
        else:
            self.vbox.addWidget(self.groupBox)

        self.done_button = QPushButton('Feito')
        self.done_button.setMinimumSize(130, 28)
        self.done_button.setMaximumSize(50, 25)
        self.done_button.clicked.connect(self.done)

        self.grid2 = QGridLayout()
        self.grid2.setAlignment(QtCore.Qt.AlignRight)
        self.grid2.addWidget(self.done_button, 0, 0)

        self.vbox.addLayout(self.grid2)
    
    def closeEvent(self, event):
        pass

    def mostrar_alfa(self):
        for i, combo in enumerate(self.fi_list):
            if combo.currentText() == 'x^α' or combo.currentText() == 'logα(x)':
                self.constant_list[i].show()
                self.aux_list[i].show()
            else:
                if not self.constant_list[i].isHidden():
                    self.constant_list[i].hide()
                    self.aux_list[i].hide()

    def done(self):
        for i in range(0, len(self.fi_list)):
            if self.fi_list[i].currentText() == '':

                # Message Box
                message = MyMessageBox('Atenção', 'Por favor, preencha\ntodas as células.', self)
                message.show()

                return 0
            if not self.constant_list[i].isHidden():
                if self.constant_list[i].text() == '':

                    # Message Box
                    message = MyMessageBox('Atenção', 'Por favor, preencha\ntodas as células.', self)
                    message.show()
                
                    return 0
        for i in range(0, len(self.fi_list)):
            for j in range(0, len(self.fi_list)):
                if j != i:
                    if self.fi_list[j].currentText() == self.fi_list[i].currentText():
                        if self.fi_list[j].currentText() == 'x^α':
                            if self.constant_list[j].text() == self.constant_list[i].text():

                                # Message Box
                                message = MyMessageBox('Atenção', 'Por favor, não repita a função.', self)
                                message.show()
                             
                                return 0
                        else:

                            # Message Box
                            message = MyMessageBox('Atenção', 'Por favor, não repita a função.', self)
                            message.show()
                         
                            return 0
        
        fi_list = list()
        constant_list = list()
        for i in range(0, len(self.fi_list)):
            fi_list.append(self.fi_list[i].currentText())
            if self.constant_list[i].text() != '':
                constant_list.append(float(self.constant_list[i].text().replace(',', '.')))
            else:
                constant_list.append('')

        self.currentWidget.setEquation(fi_list, constant_list)
        self.currentWidget.valorDeX_lineEdit.clear()
        self.currentWidget.solution_label.clear()
        self.currentWidget.numPoints_lineEdit.setFocus()
        self.setParent(None)

    def setCurrentWidget(self, _currentWidget):
        self.currentWidget = _currentWidget
