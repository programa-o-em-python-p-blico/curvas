from PyQt5.QtWidgets import QWidget, QVBoxLayout, QGridLayout, QLabel, QPushButton
from PyQt5 import QtGui, QtCore
from NewtonWidget import Newton
from MMQWidget import MMQ


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.mainWindow_text()
        self.mainWindow_bottons()

        self.newton = Newton()
        self.mmq = MMQ()

    def mainWindow_text(self):
        self.vbox = QVBoxLayout()
        self.grid = QGridLayout()

        self.numericalMethod_label = QLabel('Métodos numéricos:')
        self.numericalMethod_label.setFont(QtGui.QFont('Corbel Light', 16))
        self.numericalMethod_label.setMinimumSize(200, 70)
        self.numericalMethod_label.setAlignment(QtCore.Qt.AlignCenter)
        self.vbox.addWidget(self.numericalMethod_label)

        self.newtonMethod_label = QLabel('Interpolação\n'
                                         'por Newton ')
        self.newtonMethod_label.setFont(QtGui.QFont('Calibri Light', 11))
        self.newtonMethod_label.setStyleSheet('color:darkblue')
        self.newtonMethod_label.setMinimumSize(100, 60)

        self.arrow1_label = QLabel()
        self.arrow1_label.setPixmap(QtGui.QPixmap('arquivos/arrow3.png'))
        self.arrow1_label.setMinimumSize(80, 50)
        self.arrow1_label.setAlignment(QtCore.Qt.AlignCenter)

        self.newtonMethodDescrition_label = QLabel('Gera a curva que melhor se\n'
                                                   'aproxima dos pontos dados ')
        self.newtonMethodDescrition_label.setFont(QtGui.QFont('Calibri Light', 10))
        self.newtonMethodDescrition_label.setStyleSheet('color:black')
        self.newtonMethodDescrition_label.setMinimumSize(200, 60)

        self.mmqMethod_label = QLabel('MMQ - Método\n'
                                      'dos Mínimos\n'
                                      'Quadrados')
        self.mmqMethod_label.setFont(QtGui.QFont('Calibri Light', 11))
        self.mmqMethod_label.setStyleSheet('color:darkblue')
        self.mmqMethod_label.setMinimumSize(100, 60)

        self.arrow2_label = QLabel()
        self.arrow2_label.setPixmap(QtGui.QPixmap('arquivos/arrow3.png'))
        self.arrow2_label.setMinimumSize(80, 50)
        self.arrow2_label.setAlignment(QtCore.Qt.AlignCenter)

        self.mmqMethodDescrition_label = QLabel('Gera os coeficientes da curva dada\n'
                                                '(que descreve o fenômeno físico)\n'
                                                'de forma que ela aproxime da melhor\n'
                                                'forma possível os pontos dados')
        self.mmqMethodDescrition_label.setFont(QtGui.QFont('Calibri Light', 10))
        self.mmqMethodDescrition_label.setMinimumSize(200, 60)
        self.mmqMethodDescrition_label.setStyleSheet('color:black')

        self.grid.addWidget(self.newtonMethod_label, 0, 0)
        self.grid.addWidget(self.arrow1_label, 0, 1)
        self.grid.addWidget(self.newtonMethodDescrition_label, 0, 2)
        self.grid.addWidget(self.mmqMethod_label, 1, 0)
        self.grid.addWidget(self.arrow2_label, 1, 1)
        self.grid.addWidget(self.mmqMethodDescrition_label, 1, 2)

        self.guideText_label = QLabel('SELECIONE O PROCEDIMENTO DESEJADO')
        self.guideText_label.setFont(QtGui.QFont('Eras Light ITC', 8))
        self.guideText_label.setStyleSheet('color:black')
        self.guideText_label.setMinimumSize(300, 40)
        self.guideText_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignCenter)

        self.vbox.addItem(self.grid)
        self.vbox.addWidget(self.guideText_label)

        self.setLayout(self.vbox)

    def mainWindow_bottons(self):
        self.goToNewton_button = QPushButton('Interpolação por Newton')
        self.goToNewton_button.setFont(QtGui.QFont('Corbel Light', 18))
        self.goToNewton_button.setMinimumSize(200, 70)

        self.goToMMQ_button = QPushButton('MMQ')
        self.goToMMQ_button.setFont(QtGui.QFont('Corbel Light', 18))
        self.goToMMQ_button.setMinimumSize(200, 70)

        self.vbox.addWidget(self.goToNewton_button)
        self.vbox.addWidget(self.goToMMQ_button)
    
    def setMyWindow(self, myWindow):
        self.myWindow = myWindow
        
        self.send_MyWindow_and_MyWidget_to_Newton()
        self.send_MyWindow_and_MyWidget_to_MMQ()
    
    def send_MyWindow_and_MyWidget_to_Newton(self):
        self.newton.set_MyWindow_and_MyWidget(self.myWindow, self)

    def send_MyWindow_and_MyWidget_to_MMQ(self):
        self.mmq.set_MyWindow_and_MyWidget(self.myWindow, self)
