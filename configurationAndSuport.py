from PyQt5.QtWidgets import QDialog, QGroupBox, QGridLayout, QVBoxLayout, QHBoxLayout, QRadioButton, QPushButton, QLabel, QComboBox
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPixmap


class Decimals(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Configuração - Casas decimais'
        left = 650 
        top = 250 
        width = 200
        height = 100
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.group = QGroupBox('Escolha uma opção')

        grid = QGridLayout()
        grid.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()
        vbox.addWidget(self.group)

        self.setLayout(vbox)

        hbox = QHBoxLayout()

        self.uma = QRadioButton('Uma')
        self.duas = QRadioButton('Duas')
        self.tres = QRadioButton('Três')
        self.quatro = QRadioButton('Quatro')
        self.cinco = (QRadioButton('Cinco'))
        hbox.addWidget(self.uma)
        hbox.addWidget(self.duas)
        hbox.addWidget(self.tres)
        hbox.addWidget(self.quatro)
        hbox.addWidget(self.cinco)

        self.group.setLayout(hbox)

        apply_button = QPushButton('Aplicar')
        apply_button.clicked.connect(_self.applyDecimal)
        grid.addWidget(apply_button, 0, 0)

        vbox.addLayout(grid)


class GraphicConf(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Configuração - Gráfico'
        left = 650 
        top = 250 
        width = 250
        height = 250
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.temas = ['Solarize_Light2', 'bmh', 'classic', 'dark_background', 'ggplot', 'grayscale', 'seaborn', 'seaborn-dark', 'seaborn-ticks', 'seaborn-white', 'seaborn-whitegrid']
        self.cores = ['Azul', 'Branco', 'Verde', 'Vermelho', 'Amarelo', 'Laranja', 'Marrom', 'Preto']
        self.colors = ['blue', 'white', 'green', 'red', 'yellow', 'orange', 'brown', 'black']
        self.tFonte = ['7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']

        vbox = QVBoxLayout()

        group1 = QGroupBox(' Temas ')
        group2 = QGroupBox(' Cores / Tamanho da fonte ')

        self.grid1 = QGridLayout()

        grid2 = QGridLayout()

        grid3 = QGridLayout()
        grid3.setAlignment(QtCore.Qt.AlignRight)

        self.themes_label = QLabel('Tema: ')
        self.themes_box = QComboBox()
        self.themes_box.currentTextChanged.connect(self.changeTheme)
        self.themes_box.addItems(self.temas)

        self.image_label = QLabel()

        self.title_label = QLabel('Título: ')
        self.title1_box = QComboBox()
        self.title1_box.addItems(self.cores)
        self.title2_box = QComboBox()
        self.title2_box.addItems(self.tFonte)

        self.x_label = QLabel('Legenda em x: ')
        self.x1_box = QComboBox()
        self.x1_box.addItems(self.cores)
        self.x2_box = QComboBox()
        self.x2_box.addItems(self.tFonte)

        self.y_label = QLabel('Legenda em y: ')
        self.y1_box = QComboBox()
        self.y1_box.addItems(self.cores)
        self.y2_box = QComboBox()
        self.y2_box.addItems(self.tFonte)

        apply_button = QPushButton('Aplicar')
        apply_button.clicked.connect(_self.applyGraphicConf)

        self.grid1.addWidget(self.themes_label, 0, 0, QtCore.Qt.AlignRight)
        self.grid1.addWidget(self.themes_box, 0, 1, QtCore.Qt.AlignLeft)
        #self.grid1.addWidget(self.labelImage, 1, 0, 1, 2, QtCore.Qt.AlignCenter)

        grid2.addWidget(self.title_label, 0, 0)
        grid2.addWidget(self.title1_box, 0, 1)
        grid2.addWidget(self.title2_box, 0, 2)
        grid2.addWidget(self.x_label, 1, 0)
        grid2.addWidget(self.x1_box, 1, 1)
        grid2.addWidget(self.x2_box, 1, 2)
        grid2.addWidget(self.y_label, 2, 0)
        grid2.addWidget(self.y1_box, 2, 1)
        grid2.addWidget(self.y2_box, 2, 2)

        grid3.addWidget(apply_button, 0, 0)

        group1.setLayout(self.grid1)
        group2.setLayout(grid2)

        vbox.addWidget(group1)
        vbox.addWidget(group2)
        vbox.addLayout(grid3)
        self.setLayout(vbox)

    def changeTheme(self):
        for i, tema in enumerate(self.temas):
            if self.themes_box.currentText() == tema:
                pixmap = QPixmap(f'arquivos/tema{i}.png')
                self.image_label = QLabel()
                self.image_label.setPixmap(pixmap)
                self.image_label.setFixedSize(218, 150)
                self.grid1.addWidget(self.image_label, 1, 0, 1, 2, QtCore.Qt.AlignCenter)


class Suport(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        #self(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint)
        #self.setWindowFlags
        title = 'Suporte'
        left = 650
        top = 250 
        width = 200
        height = 80
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        vbox =QVBoxLayout()
        label = QLabel('Envie sua dúvida para: rafa10031999@gmail.com')
        vbox.addWidget(label)

        self.setLayout(vbox)


class Theme(QDialog):
    def __init__(self, _self):
        super().__init__(_self, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        title = 'Configuração - Tema'
        left = 650 
        top = 250 
        width = 200
        height = 100
        icone = 'arquivos/icone.ico'

        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(icone))
        self.setGeometry(left, top, width, height)

        self.setWindowModality(QtCore.Qt.ApplicationModal)  # Impede o retorno para a MainWindow sem fechar a QDialog

        self.group = QGroupBox('Escolha uma opção')

        grid = QGridLayout()
        grid.setAlignment(QtCore.Qt.AlignRight)

        vbox = QVBoxLayout()
        vbox.addWidget(self.group)

        self.setLayout(vbox)

        hbox = QHBoxLayout()

        self.windows = QRadioButton('Windows')
        self.windowsvista = QRadioButton('Windows Vista')
        self.fusion = QRadioButton('Fusion')

        hbox.addWidget(self.windows)
        hbox.addWidget(self.windowsvista)
        hbox.addWidget(self.fusion)

        self.group.setLayout(hbox)

        apply_button = QPushButton('Aplicar')
        apply_button.clicked.connect(_self.applyThemeConf)
        grid.addWidget(apply_button, 0, 0)

        vbox.addLayout(grid)
